#version 430 core
#extension GL_EXT_texture_array : enable

struct Ray
{
    vec3 origin;
    vec3 direction;
	vec3 reflectionFactor;
	uint isActive;
};

struct Intersection
{
	float t;
	float u, v;
	int i0, i1, i2;
	uint materialIndex;
};

struct Vertex
{
	vec3 position;
	vec3 normal;
	vec2 texCoord;
};

struct Material
{
	uint baseNode, nodeCount;
	int diffuseTextureIndex, normalMapTextureIndex;
	vec4 color;
};

struct OctreeNode
{
	uint baseVertex, vertexCount;
	uint childOffset;
	vec3 bottomLeftAABBCorner, topRightAABBCorner;
};

struct Camera
{
	vec3 position;
	vec3 right;
	vec3 up;
	vec3 look;
	float fov;
};

struct PointLight
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	vec3 position;
	float range;

	vec3 attenuation;
};

struct DirectionalLight
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	vec3 direction;
};

struct Sphere
{
    vec3 center;
    float radius;
};

struct Triangle
{
	vec3 position0;
	vec3 position1;
	vec3 position2;
};

struct AABB
{
	vec3 bottomLeftCorner;
	vec3 topRightCorner;
};

// Ray data
layout (std430, binding = 0) buffer RayBuffer
{
	Ray rays[];
};
// Intersection data
layout (std430, binding = 1) buffer IntersectionBuffer
{
	Intersection intersections[];
};
// Vertex data
layout (std430, binding = 2) buffer VertexBuffer
{
	Vertex vertices[];
};
// Mesh data
layout (std430, binding = 3) buffer MaterialBuffer
{
	Material materials[];
};
// Octree data
layout (std430, binding = 4) buffer OctreeNodeBuffer
{
	OctreeNode octreeNodes[];
};
// Point light data
layout (std430, binding = 5) buffer PointLightBuffer
{
	PointLight pointLights[];
};
// Directional light data
layout (std430, binding = 6) buffer DirectionalLightBuffer
{
	DirectionalLight directionalLights[];
};
// Output texture
layout (rgba32f, binding = 7) uniform image2D uOutput;

uniform sampler2DArray uTextures;

uniform int uClientWidth;
uniform int uClientHeight;

uniform uint uPointLightCount;

float RaySphereIntersect(Ray ray, Sphere sphere)
{
	vec3 l = sphere.center - ray.origin;
	float s = dot(l, ray.direction);
	float l2 = dot(l, l);
	float r2 = sphere.radius * sphere.radius;
	if (s < 0 && l2 > r2)
		return -1.0;
	float m2 = l2 - s * s;
	if (m2 > r2)
		return -1.0;
	float q = sqrt(r2 - m2);
	float t;
	if (l2 > r2)
		t = s - q;
	else
		t = s + q;
	return t;
}

const float EPSILON = 0.000001;
float RayTriangleIntersect(Ray ray, Triangle triangle,
						   out float u, out float v)
{
	vec3 e1 = triangle.position1 - triangle.position0;
	vec3 e2 = triangle.position2 - triangle.position0;
	vec3 q = cross(ray.direction, e2);
	float a = dot(e1, q);
	if (a > -EPSILON && a < EPSILON)
		return -1.0;
	float f = 1 / a;
	vec3 s = ray.origin - triangle.position0;
	u = f * dot(s, q);
	if (u < 0.0)
		return -1.0;
	vec3 r = cross(s, e1);
	v = f * dot(ray.direction, r);
	if (v < 0.0 || u + v > 1.0)
		return -1.0;
	float t = f * dot(e2, r);
	return t;
}

float RayAABBIntersect(Ray ray, AABB aabb)
{
	if (ray.origin.x >= aabb.bottomLeftCorner.x && ray.origin.x <= aabb.topRightCorner.x &&
		ray.origin.y >= aabb.bottomLeftCorner.y && ray.origin.y <= aabb.topRightCorner.y &&
		ray.origin.z >= aabb.bottomLeftCorner.z && ray.origin.z <= aabb.topRightCorner.z)
		return 0.001;

	vec3 invDirection = 1.0 / ray.direction;

	float t1 = (aabb.bottomLeftCorner.x - ray.origin.x) * invDirection.x;
	float t2 = (aabb.topRightCorner.x	- ray.origin.x) * invDirection.x;
	float t3 = (aabb.bottomLeftCorner.y - ray.origin.y) * invDirection.y;
	float t4 = (aabb.topRightCorner.y	- ray.origin.y) * invDirection.y;
	float t5 = (aabb.bottomLeftCorner.z - ray.origin.z) * invDirection.z;
	float t6 = (aabb.topRightCorner.z	- ray.origin.z) * invDirection.z;

	float tMin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
	float tMax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));

	if (tMax < 0.0 || tMin > tMax)
		return -1.0;
	return tMin;
}

bool IsRayIntersectingWithMesh(Ray ray, float maxDistance)
{
	uint nodeIndex;
	uint nodeCount;
	OctreeNode node;
	AABB aabb;

	uint materialCount = materials.length();
	for (uint i = 0; i < materialCount; ++i)
	{
		Material material = materials[i];

		nodeIndex = material.baseNode;
		nodeCount = material.nodeCount;

		while (true)
		{
			node = octreeNodes[nodeIndex];

			aabb.bottomLeftCorner = node.bottomLeftAABBCorner;
			aabb.topRightCorner = node.topRightAABBCorner;
			float aabbT = RayAABBIntersect(ray, aabb);

			if (aabbT < 0.0)
				nodeIndex += node.childOffset;
			else
			{
				if (node.vertexCount > 0)
				{
					for (uint j = node.baseVertex; j < node.vertexCount; j += 3)
					{
						uint i0 = j;
						uint i1 = j + 1;
						uint i2 = j + 2;

						Triangle triangle;
						triangle.position0 = vertices[i0].position;
						triangle.position1 = vertices[i1].position;
						triangle.position2 = vertices[i2].position;

						float u, v;
						float t = RayTriangleIntersect(ray, triangle, u, v);
						if (t > 0.00001 && t <= maxDistance)
							return true;
					}
				}

				nodeIndex++;
			}

			if (nodeIndex >= nodeCount)
				break;
		}
	}

	return false;
}

void CalculatePointLight(PointLight pointLight, vec3 position, vec3 normal,
						 vec3 oldRayDirection, vec3 newRayDirection,
						 out vec4 ambient, out vec4 diffuse, out vec4 specular)
{
	ambient  = pointLight.ambient;
	diffuse  = vec4(0.0, 0.0, 0.0, 0.0);
	specular = vec4(0.0, 0.0, 0.0, 0.0);

	vec3 direction = position - pointLight.position;
	float d = length(direction);
	if (d > pointLight.range)
		return;
	direction = normalize(direction);

	Ray ray;
	ray.origin = position;
	ray.direction = -direction;

	if (IsRayIntersectingWithMesh(ray, d))
		return;

	float diffuseFactor = max(0.0, -dot(normal, direction));
	diffuse = diffuseFactor * pointLight.diffuse;

	float specularFactor = max(0.0, -dot(direction, newRayDirection));
	specular = specularFactor * pointLight.specular;

	float attenuation = 1.0 / dot(pointLight.attenuation, vec3(1.0, d, d * d));
	diffuse  *= attenuation;
	specular *= attenuation;
}

void CalculateDirectionalLight(DirectionalLight directionalLight, vec3 position, vec3 normal, vec3 newRayDirection,
							   out vec4 ambient, out vec4 diffuse, out vec4 specular)
{
	ambient  = directionalLight.ambient;
	diffuse  = vec4(0.0, 0.0, 0.0, 0.0);
	specular = vec4(0.0, 0.0, 0.0, 0.0);

	Ray ray;
	ray.origin = position;
	ray.direction = -directionalLight.direction;

	if (IsRayIntersectingWithMesh(ray, 1000.0))
		return;

	float diffuseFactor = max(0.0, -dot(normal, directionalLight.direction));
	diffuse = diffuseFactor * directionalLight.diffuse;

	float specularFactor = max(0.0, -dot(directionalLight.direction, newRayDirection));
	specular = specularFactor * directionalLight.specular;
}

vec3 CalculateNormal(Intersection intersection)
{
	vec3 normal0 = vertices[intersection.i0].normal;
	vec3 normal1 = vertices[intersection.i1].normal;
	vec3 normal2 = vertices[intersection.i2].normal;
	float t = 1.0 - intersection.u - intersection.v;
	return normalize(vec3(normal0.x * t + normal1.x * intersection.u + normal2.x * intersection.v,
						  normal0.y * t + normal1.y * intersection.u + normal2.y * intersection.v,
						  normal0.z * t + normal1.z * intersection.u + normal2.z * intersection.v));
}

vec2 CalculateTexCoord(Intersection intersection)
{
	vec2 texCoord0 = vertices[intersection.i0].texCoord;
	vec2 texCoord1 = vertices[intersection.i1].texCoord;
	vec2 texCoord2 = vertices[intersection.i2].texCoord;
	return intersection.u * texCoord1 + intersection.v * texCoord2 + (1.0 - (intersection.u + intersection.v)) * texCoord0;
}

layout (local_size_x = 16, local_size_y = 16, local_size_z = 1) in;
void main()
{
    uint x = gl_GlobalInvocationID.x;
    uint y = gl_GlobalInvocationID.y;
	int p = int(y) * uClientWidth + int(x);

    if (int(x) < uClientWidth && int(y) < uClientHeight)
	{
		Ray ray = rays[p];
		Intersection intersection = intersections[p];

		// Hit
		if (intersection.t > 0.0)
		{
			Material material = materials[intersection.materialIndex];

			vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
			vec3 position = ray.origin + intersection.t * ray.direction;
			vec3 normal = CalculateNormal(intersection);
			vec2 texCoord = CalculateTexCoord(intersection);

			// Normal mapping
			int normalIndex = material.normalMapTextureIndex;
			if (normalIndex >= 0)
			{
				vec3 normalColor = texture2DArray(uTextures, vec3(texCoord, normalIndex)).xyz;
				if (length(normalColor) < 0.1)
					normalColor = vec3(0.5, 0.5, 1.0);
				normalColor = 2.0 * normalColor - 1.0;

				vec3 position0 = vertices[intersection.i0].position;
				vec3 position1 = vertices[intersection.i1].position;
				vec3 position2 = vertices[intersection.i2].position;
				vec3 positionEdge0 = position1 - position0;
				vec3 positionEdge1 = position2 - position0;

				vec2 texCoord0 = vertices[intersection.i0].texCoord;
				vec2 texCoord1 = vertices[intersection.i1].texCoord;
				vec2 texCoord2 = vertices[intersection.i2].texCoord;
				vec2 texCoordEdge0 = texCoord1 - texCoord0;
				vec2 texCoordEdge1 = texCoord2 - texCoord0;
				const float cp = texCoordEdge0.y * texCoordEdge1.x - texCoordEdge0.x * texCoordEdge1.y;
				
				if (cp != 0.0)
				{
					const float multiplier = 1.0 / cp;

					vec3 tangent = (positionEdge1 * texCoordEdge0.y - positionEdge0 * texCoordEdge1.y) * multiplier;
					vec3 bitangent = (positionEdge1 * texCoordEdge0.x - positionEdge0 * texCoordEdge1.x) * multiplier;
					tangent -= normal * dot(normal, tangent);
					tangent = normalize(tangent);
					bitangent -= normal * dot(normal, bitangent);
					bitangent -= tangent * dot(tangent, bitangent);
					bitangent = normalize(bitangent);
					normal = normalColor.z * normal + normalColor.x * tangent - normalColor.y * bitangent;
				}
			}

			if (dot(ray.direction, normal) > 0.0)
				normal = -normal;
			vec3 newRayDirection = normalize(reflect(ray.direction, normal));

			vec4 ambient = vec4(0.0, 0.0, 0.0, 0.0);
			vec4 diffuse = vec4(0.0, 0.0, 0.0, 0.0);
			vec4 specular = vec4(0.0, 0.0, 0.0, 0.0);

			vec4 a, d, s;
			//int pointLightCount = pointLights.length();
			int pointLightCount = int(uPointLightCount);
			for (int i = 0; i < pointLightCount; ++i)
			{
				CalculatePointLight(pointLights[i], position, normal,
									ray.direction, newRayDirection,
									a, d, s);
				ambient += a;
				diffuse += d;
				specular += s;
			}
			int directionalLightCount = directionalLights.length();
			for (int i = 0; i < directionalLightCount; ++i)
			{
				CalculateDirectionalLight(directionalLights[i], position, normal, newRayDirection,
										  a, d, s);
				ambient += a;
				diffuse += d;
				specular += s;
			}

			int diffuseIndex = material.diffuseTextureIndex;
			vec4 diffuseColor = diffuseIndex < 0 ? material.color : texture2DArray(uTextures, vec3(texCoord, diffuseIndex));
			color = diffuseColor * (ambient + diffuse) + specular;
			color *= vec4(ray.reflectionFactor, 1.0);

			ray.origin = position;
			ray.direction = newRayDirection;
			ray.reflectionFactor *= vec3(0.6, 0.6, 0.6); //specular.xyz;
			rays[p] = ray;

			vec4 outputColor = imageLoad(uOutput, ivec2(x, y));
			outputColor += color;
			imageStore(uOutput, ivec2(x, y), outputColor);
		}
		// No hit
		else
		{
			ray.isActive = 0;
			rays[p] = ray;
		}
    } 
}