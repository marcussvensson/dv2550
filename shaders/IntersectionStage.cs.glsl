#version 430 core

struct Ray
{
    vec3 origin;
    vec3 direction;
	vec3 reflectionFactor;
	uint isActive;
};

struct Intersection
{
	float t;
	float u, v;
	uint i0, i1, i2;
	uint materialIndex;
};

struct Vertex
{
	vec3 position;
	vec3 normal;
	vec2 texCoord;
};

struct OctreeNode
{
	uint baseVertex, vertexCount;
	uint childOffset;
	vec3 bottomLeftAABBCorner, topRightAABBCorner;
};

struct Material
{
	uint baseNode, nodeCount;
	int diffuseTextureIndex, normalMapTextureIndex;
	vec4 color;
};

struct Sphere
{
    vec3 center;
    float radius;
};

struct Triangle
{
	vec3 position0;
	vec3 position1;
	vec3 position2;
};

struct AABB
{
	vec3 bottomLeftCorner;
	vec3 topRightCorner;
};

// Ray data
layout (std430, binding = 0) buffer RayBuffer
{
	Ray rays[];
};
// Intersection data
layout (std430, binding = 1) buffer IntersectionBuffer
{
	Intersection intersections[];
};
// Vertex data
layout (std430, binding = 2) buffer VertexBuffer
{
	Vertex vertices[];
};
// Material data
layout (std430, binding = 3) buffer MaterialBuffer
{
	Material materials[];
};
// Octree data
layout (std430, binding = 4) buffer OctreeNodeBuffer
{
	OctreeNode octreeNodes[];
};

uniform int uClientWidth;
uniform int uClientHeight;

float RaySphereIntersect(Ray ray, Sphere sphere)
{
	vec3 l = sphere.center - ray.origin;
	float s = dot(l, ray.direction);
	float l2 = dot(l, l);
	float r2 = sphere.radius * sphere.radius;
	if (s < 0 && l2 > r2)
		return -1.0;
	float m2 = l2 - s * s;
	if (m2 > r2)
		return -1.0;
	float q = sqrt(r2 - m2);
	float t;
	if (l2 > r2)
		t = s - q;
	else
		t = s + q;
	return t;
}

const float EPSILON = 0.0000001;
float RayTriangleIntersect(Ray ray, Triangle triangle,
						   out float u, out float v)
{
	vec3 e1 = triangle.position1 - triangle.position0;
	vec3 e2 = triangle.position2 - triangle.position0;
	vec3 q = cross(ray.direction, e2);
	float a = dot(e1, q);
	if (a > -EPSILON && a < EPSILON)
		return -1.0;
	float f = 1 / a;
	vec3 s = ray.origin - triangle.position0;
	u = f * dot(s, q);
	if (u < -EPSILON)
		return -1.0;
	vec3 r = cross(s, e1);
	v = f * dot(ray.direction, r);
	if (v < -EPSILON || u + v > 1.0 + EPSILON)
		return -1.0;
	float t = f * dot(e2, r);
	return t;
}

float RayAABBIntersect(Ray ray, AABB aabb)
{
	if (ray.origin.x >= aabb.bottomLeftCorner.x && ray.origin.x <= aabb.topRightCorner.x &&
		ray.origin.y >= aabb.bottomLeftCorner.y && ray.origin.y <= aabb.topRightCorner.y &&
		ray.origin.z >= aabb.bottomLeftCorner.z && ray.origin.z <= aabb.topRightCorner.z)
		return 0.001;

	vec3 invDirection = 1.0 / ray.direction;

	float t1 = (aabb.bottomLeftCorner.x - ray.origin.x) * invDirection.x;
	float t2 = (aabb.topRightCorner.x	- ray.origin.x) * invDirection.x;
	float t3 = (aabb.bottomLeftCorner.y - ray.origin.y) * invDirection.y;
	float t4 = (aabb.topRightCorner.y	- ray.origin.y) * invDirection.y;
	float t5 = (aabb.bottomLeftCorner.z - ray.origin.z) * invDirection.z;
	float t6 = (aabb.topRightCorner.z	- ray.origin.z) * invDirection.z;

	float tMin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
	float tMax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));

	if (tMax < 0.0 || tMin > tMax)
		return -1.0;
	return tMin;
}

Intersection RayOctreeIntersect(Ray ray)
{
	Intersection closestIntersection;
	closestIntersection.t = -1.0;

	uint nodeIndex;
	uint nodeCount;
	OctreeNode node;
	AABB aabb;

	uint materialCount = materials.length();
	for (uint i = 0; i < materialCount; ++i)
	{
		Material material = materials[i];

		nodeIndex = material.baseNode;
		nodeCount = material.nodeCount;

		while (true)
		{
			node = octreeNodes[nodeIndex];

			aabb.bottomLeftCorner = node.bottomLeftAABBCorner;
			aabb.topRightCorner = node.topRightAABBCorner;
			float aabbT = RayAABBIntersect(ray, aabb);

			if (aabbT < 0.0)
				nodeIndex += node.childOffset;
			else
			{
				if (node.vertexCount > 0)
				{
					for (uint j = node.baseVertex; j < node.vertexCount; j += 3)
					{
						uint i0 = j;
						uint i1 = j + 1;
						uint i2 = j + 2;

						Triangle triangle;
						triangle.position0 = vertices[i0].position;
						triangle.position1 = vertices[i1].position;
						triangle.position2 = vertices[i2].position;

						float u, v;
						float t = RayTriangleIntersect(ray, triangle, u, v);
						if (t > 0.00001 && (closestIntersection.t < 0.0 || t < closestIntersection.t))
						{
							closestIntersection.t = t;
							closestIntersection.u = u;
							closestIntersection.v = v;
							closestIntersection.i0 = i0;
							closestIntersection.i1 = i1;
							closestIntersection.i2 = i2;
							closestIntersection.materialIndex = i;
						}
					}
				}

				nodeIndex++;
			}

			if (nodeIndex >= nodeCount)
				break;
		}
	}

	return closestIntersection;
}

layout (local_size_x = 16, local_size_y = 16, local_size_z = 1) in;
void main()
{
    uint x = gl_GlobalInvocationID.x;
    uint y = gl_GlobalInvocationID.y;
	int p = int(y) * uClientWidth + int(x);

    if (int(x) < uClientWidth && int(y) < uClientHeight)
	{
        Ray ray = rays[p];

		Intersection closestIntersection;
		closestIntersection.t = -1.0;
		if (ray.isActive == 1)
			closestIntersection = RayOctreeIntersect(ray);
		intersections[p] = closestIntersection;
    } 
}