#version 430 core

struct Camera
{
	vec3 position;
	vec3 right;
	vec3 up;
	vec3 look;
	float fov;
};

struct Ray
{
    vec3 origin;
    vec3 direction;
	vec3 reflectionFactor;
	uint isActive;
};

// Ray data
layout (std430, binding = 0) buffer RayBuffer
{
	Ray rays[];
};

uniform int uClientWidth;
uniform int uClientHeight;
uniform Camera uCamera;

Ray CalculatePrimaryRay(uint x, uint y)
{
	float halfWidth = 0.5 * float(uClientWidth);
	float halfHeight = 0.5 * float(uClientHeight);

	float xFactor = float(x) - halfWidth;
	float yFactor = float(y) - halfHeight;
	float zFactor = halfWidth / tan(0.5 * uCamera.fov);

	vec3 right = uCamera.right * xFactor;
	vec3 up = uCamera.up * yFactor;
	vec3 look = uCamera.look * zFactor;
	vec3 direction = normalize(right + up + look);

    Ray ray;
	ray.origin = uCamera.position;
	ray.direction = normalize(right + up + look);
	ray.reflectionFactor = vec3(1.0, 1.0, 1.0);
	ray.isActive = 1;
	return ray;
}

layout (local_size_x = 16, local_size_y = 16, local_size_z = 1) in;
void main()
{
    uint x = gl_GlobalInvocationID.x;
    uint y = gl_GlobalInvocationID.y;
	int p = int(y) * uClientWidth + int(x);

    if (x < uClientWidth && y < uClientHeight)
	{
        Ray ray = CalculatePrimaryRay(x, y);
		rays[p] = ray;
    }
}