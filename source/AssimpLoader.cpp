#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include "AssimpLoader.h"

glm::vec3 GetGlmVec3(const aiVector3D& v)
{
	return glm::vec3(v.x, v.y, v.z);
}

glm::mat4 GetGlmMat4(const aiMatrix4x4& m)
{
	glm::mat4 r;
	r[0][0] = m.a1; r[1][0] = m.a2; r[2][0] = m.a3; r[3][0] = m.a4;
	r[0][1] = m.b1; r[1][1] = m.b2; r[2][1] = m.b3; r[3][1] = m.b4;
	r[0][2] = m.c1; r[1][2] = m.c2; r[2][2] = m.c3; r[3][2] = m.c4;
	r[0][3] = m.d1; r[1][3] = m.d2; r[2][3] = m.d3; r[3][3] = m.d4;
	return r;
}

glm::quat GetGlmQuat(const aiQuaternion& q)
{
	return glm::quat(q.w, q.x, q.y, q.z);
}

AssimpLoader& AssimpLoader::GetInstance()
{
	static AssimpLoader instance;
	return instance;
}

AssimpLoader::AssimpLoader() { }

void AssimpLoader::Load(const std::string& filepath, MeshData* data)
{
	if (data == nullptr || data == NULL)
		throw std::exception("Assimp data pointer equals null");

	Assimp::Importer importer;
	m_scene = importer.ReadFile(filepath.c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);
	if (!m_scene)
		throw std::exception(std::string("Unable to load mesh from file \"" + filepath + "\". Error: " + importer.GetErrorString()).c_str());

	// Calculate mesh ranges
	unsigned int vertexCount = 0;
	unsigned int indexCount = 0;
	data->ranges.resize(m_scene->mNumMeshes);
	for (unsigned int i = 0; i < m_scene->mNumMeshes; i++)
	{
		data->ranges[i].materialIndex = m_scene->mMeshes[i]->mMaterialIndex;
		data->ranges[i].vertexCount = m_scene->mMeshes[i]->mNumVertices;
		data->ranges[i].indexCount = m_scene->mMeshes[i]->mNumFaces * 3;
		data->ranges[i].startVertex = vertexCount;
		data->ranges[i].startIndex = indexCount;

		vertexCount += data->ranges[i].vertexCount;
		indexCount += data->ranges[i].indexCount;

		data->ranges[i].useBoundingBox = true;
	}

	// Allocate data
	data->positions.reserve(vertexCount);
	data->normals.reserve(vertexCount);
	data->texCoords.reserve(vertexCount);
	if (m_scene->HasAnimations())
	{
		data->boneIndexes.resize(vertexCount);
		data->boneWeights.resize(vertexCount, glm::vec4(0.f));
	}
	data->indices.reserve(indexCount);

	// Load mesh data
	for (unsigned int i = 0; i < m_scene->mNumMeshes; i++)
	{
		const aiMesh* pAiMesh = m_scene->mMeshes[i];
		LoadVertices(pAiMesh, data,
					 &data->ranges[i].minBoundingBox, &data->ranges[i].maxBoundingBox);
		LoadIndices(pAiMesh, data);
		LoadBones(pAiMesh, data, data->ranges[i].startVertex);
	}

	LoadMaterials(filepath, data);

	LoadNodeHierarchy(m_scene->mRootNode, data->root);
	data->boneCount = unsigned int(m_boneMap.size());
	data->animationDurations.resize(m_scene->mNumAnimations);
	// Set duration for animations
	for (unsigned int i = 0; i < m_scene->mNumAnimations; i++)
	{
		data->animationDurations[i] = float(m_scene->mAnimations[i]->mDuration);
	}

	data->model = GetGlmMat4(m_scene->mRootNode->mTransformation);
	data->modelInverse = glm::inverse(data->model);

	m_boneMap.clear();
	m_boneOffsets.clear();
	m_scene = nullptr;
}

void AssimpLoader::LoadMaterials(const std::string& filepath, MeshData* data)
{
	std::string::size_type slashPosition = filepath.find_last_of("/");
	std::string directory;
	if (slashPosition == std::string::npos)
	{
		directory = ".";
	}
	else if (slashPosition == 0)
	{
		directory = "/";
	}
	else
	{
		directory = filepath.substr(0, slashPosition);
	}

	data->materials.resize(m_scene->mNumMaterials);
	for (unsigned int i = 0; i < m_scene->mNumMaterials; i++)
	{
		const aiMaterial* material = m_scene->mMaterials[i];

		LoadTextureFilepath(material, aiTextureType_DIFFUSE, directory,
							data->materials[i].diffuseTextureIndex, data->textureFilepaths);
		LoadTextureFilepath(material, aiTextureType_HEIGHT, directory,
							data->materials[i].normalMapTextureIndex, data->textureFilepaths);
	}
}

void AssimpLoader::LoadTextureFilepath(const aiMaterial* pAiMaterial, aiTextureType aiType, const std::string& directory,
									   int& index, std::vector<std::string>& filepaths)
{
	if (pAiMaterial->GetTextureCount(aiType) > 0)
	{
		aiString aiFilepath;
		if (pAiMaterial->GetTexture(aiType, 0, &aiFilepath, nullptr, nullptr, nullptr, nullptr, nullptr) == AI_SUCCESS)
		{
			std::string filename(aiFilepath.data);
			if (filename.substr(0, 2) == ".\\")
			{
				filename = filename.substr(2, filename.size() - 2);
			}
			size_t lastSlashPosition = filename.find_last_of("\\/");
			if (std::string::npos != lastSlashPosition)
			{
				filename.erase(0, lastSlashPosition + 1);
			}
			std::string filepath = directory + "/" + filename;

			unsigned int i = 0;
			const unsigned int filepathCount = unsigned int(filepaths.size());
			while (i < filepathCount)
			{
				if (filepath == filepaths[i])
					break;
				i++;
			}
			index = i;

			if (i == filepathCount)
			{
				filepaths.push_back(filepath);
			}
		}
	}
	else
	{
		index = -1;
	}
}

void AssimpLoader::LoadVertices(const aiMesh* pAiMesh, MeshData* data,
								glm::vec3* minBoundingBox, glm::vec3* maxBoundingBox)
{
	const bool hasPositions = pAiMesh->HasPositions();
	const bool hasNormals = pAiMesh->HasNormals();
	const bool hasTexCoords = pAiMesh->HasTextureCoords(0);

	for (unsigned int i = 0; i < pAiMesh->mNumVertices; i++)
	{
		if (hasPositions)
		{
			const aiVector3D* pAiPosition = &(pAiMesh->mVertices[i]);
			data->positions.push_back(glm::vec3(pAiPosition->x, pAiPosition->y, pAiPosition->z));

			// Calculate bounding box values
			if (pAiPosition->x < minBoundingBox->x) minBoundingBox->x = pAiPosition->x;
			if (pAiPosition->y < minBoundingBox->y) minBoundingBox->y = pAiPosition->y;
			if (pAiPosition->z < minBoundingBox->z) minBoundingBox->z = pAiPosition->z;
			if (pAiPosition->x > maxBoundingBox->x) maxBoundingBox->x = pAiPosition->x;
			if (pAiPosition->y > maxBoundingBox->y) maxBoundingBox->y = pAiPosition->y;
			if (pAiPosition->z > maxBoundingBox->z) maxBoundingBox->z = pAiPosition->z;
		}
		if (hasNormals)
		{
			const aiVector3D* pAiNormal = &(pAiMesh->mNormals[i]);
			data->normals.push_back(glm::normalize(glm::vec3(pAiNormal->x, pAiNormal->y, pAiNormal->z)));
		}
		if (hasTexCoords)
		{
			const aiVector3D* pAiTexCoord = &(pAiMesh->mTextureCoords[0][i]);
			data->texCoords.push_back(glm::vec2(pAiTexCoord->x, pAiTexCoord->y));
		}
	}
}

void AssimpLoader::LoadIndices(const aiMesh* pAiMesh, MeshData* data)
{
	const bool hasIndices = pAiMesh->HasFaces();

	for (unsigned int i = 0; i < pAiMesh->mNumFaces; i++)
	{
		const aiFace& face = pAiMesh->mFaces[i];
		assert(face.mNumIndices == 3);
		for (unsigned int j = 0; j < 3; j++)
		{
			data->indices.push_back(face.mIndices[j]);
		}
	}
}

void AssimpLoader::LoadBones(const aiMesh* pAiMesh, MeshData* data, unsigned int vertexOffset)
{
	if (pAiMesh->HasBones())
	{
		for (unsigned int i = 0; i < pAiMesh->mNumBones; i++)
		{
			unsigned int boneIndex = 0;
			std::string boneName(pAiMesh->mBones[i]->mName.data);

			if (m_boneMap.find(boneName) == m_boneMap.end())
			{
				boneIndex = (unsigned int)m_boneMap.size();
				m_boneMap[boneName] = boneIndex;
				m_boneOffsets.push_back(GetGlmMat4(pAiMesh->mBones[i]->mOffsetMatrix));
			}
			else
			{
				boneIndex = m_boneMap[boneName];
			}

			// Load vertex bone weights and indexes
			for (unsigned int j = 0; j < pAiMesh->mBones[i]->mNumWeights; j++)
			{
				const unsigned int vertexIndex = vertexOffset + pAiMesh->mBones[i]->mWeights[j].mVertexId;
				const float weight = pAiMesh->mBones[i]->mWeights[j].mWeight;
				for (unsigned int k = 0; k < 4; k++)
				{
					if (data->boneWeights[vertexIndex][k] == 0.f)
					{
						data->boneWeights[vertexIndex][k] = weight;
						data->boneIndexes[vertexIndex][k] = boneIndex;
						break;
					}
				}
			}
		}
	}
}

void AssimpLoader::LoadNodeHierarchy(const aiNode* pAiNode, MeshData::Node& node)
{
	std::string nodeName(pAiNode->mName.data);
	node.index = m_boneMap.find(nodeName) != m_boneMap.end() ? m_boneMap[nodeName] : 0;

	node.transformation = GetGlmMat4(pAiNode->mTransformation);
	node.offset = m_boneMap.find(nodeName) != m_boneMap.end() ? m_boneOffsets[m_boneMap[nodeName]] : glm::mat4(1.f);

	node.hasBone = m_boneMap.find(nodeName) != m_boneMap.end();

	node.animations.resize(m_scene->mNumAnimations);
	for (unsigned int i = 0; i < m_scene->mNumAnimations; i++)
	{
		const aiNodeAnim* pAiNodeAnim = FindAiNodeAnim(m_scene->mAnimations[i], nodeName);
		node.animations[i].valid = pAiNodeAnim != nullptr;
		if (node.animations[i].valid)
		{
			LoadTranslationKeys(pAiNodeAnim, node.animations[i].translationKeys);
			LoadRotationKeys(pAiNodeAnim, node.animations[i].rotationKeys);
			LoadScalingKeys(pAiNodeAnim, node.animations[i].scalingKeys);
		}
	}

	node.children.resize(pAiNode->mNumChildren);
	for (unsigned int i = 0; i < pAiNode->mNumChildren; i++)
	{
		LoadNodeHierarchy(pAiNode->mChildren[i], node.children[i]);
	}
}

const aiNodeAnim* AssimpLoader::FindAiNodeAnim(const aiAnimation* animation, const std::string& nodeName)
{
	for (unsigned int i = 0; i < animation->mNumChannels; i++)
	{
		const aiNodeAnim* nodeAnim = animation->mChannels[i];
		if (std::string(nodeAnim->mNodeName.data) == nodeName)
		{
			return nodeAnim;
		}
	}
	return nullptr;
}

void AssimpLoader::LoadTranslationKeys(const aiNodeAnim* pAiNodeAnim, std::vector<MeshData::Node::Animation::TranslationKey>& translationKeys)
{
	translationKeys.resize(pAiNodeAnim->mNumPositionKeys);
	for (unsigned int i = 0; i < pAiNodeAnim->mNumPositionKeys; i++)
	{
		translationKeys[i].time = float(pAiNodeAnim->mPositionKeys[i].mTime);
		translationKeys[i].value = GetGlmVec3(pAiNodeAnim->mPositionKeys[i].mValue);
	}
}

void AssimpLoader::LoadRotationKeys(const aiNodeAnim* pAiNodeAnim, std::vector<MeshData::Node::Animation::RotationKey>& rotationKeys)
{
	rotationKeys.resize(pAiNodeAnim->mNumRotationKeys);
	for (unsigned int i = 0; i < pAiNodeAnim->mNumRotationKeys; i++)
	{
		rotationKeys[i].time = float(pAiNodeAnim->mRotationKeys[i].mTime);
		rotationKeys[i].value = GetGlmQuat(pAiNodeAnim->mRotationKeys[i].mValue);
	}
}

void AssimpLoader::LoadScalingKeys(const aiNodeAnim* pAiNodeAnim, std::vector<MeshData::Node::Animation::ScalingKey>& scalingKeys)
{
	scalingKeys.resize(pAiNodeAnim->mNumScalingKeys);
	for (unsigned int i = 0; i < pAiNodeAnim->mNumScalingKeys; i++)
	{
		scalingKeys[i].time = float(pAiNodeAnim->mScalingKeys[i].mTime);
		scalingKeys[i].value = GetGlmVec3(pAiNodeAnim->mScalingKeys[i].mValue);
	}
}