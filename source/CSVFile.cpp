#include "CSVFile.h"

CSVFile::~CSVFile()
{
	m_file.close();
}

void CSVFile::Init(const std::string& filepath, bool append)
{
	std::ios_base::open_mode om = std::fstream::out;
	if (append)
		om |= std::fstream::app;
	m_file.open(filepath, om);
}

void CSVFile::Write(const std::string& text)
{
	m_file << text;
}