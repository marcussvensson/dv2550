#include <stb_image.h>

#include "GLTextureArray.h"

GLTextureArray::~GLTextureArray()
{
	glDeleteTextures(1, &m_texture);
}

void GLTextureArray::Init(std::vector<std::string> filepaths)
{
	if (filepaths.empty())
	{
		std::string errorMessage = "Filepath vector is empty";
		throw std::exception(errorMessage.c_str());
	}

	struct Image
	{
		unsigned char* data;
		int width, height, elementCount;
	};

	const unsigned int textureCount = (unsigned int)filepaths.size();
	std::vector<Image> images(textureCount);
	GLint format = GL_NONE;
	int largestWidth = 0,
		largestHeight = 0;
	for (unsigned int i = 0; i < textureCount; i++)
	{
		// Open file
		FILE* file = fopen(filepaths[i].c_str(), "rb");
		if (!file)
		{
			std::string errorMessage = "Unable to open file \"" + filepaths[i] + "\"";
			throw std::exception(errorMessage.c_str());
		}

		// Load image data
		images[i].data = stbi_load_from_file(file, &images[i].width, &images[i].height, &images[i].elementCount, 0);

		// Close file
		fclose(file);

		if (format == GL_NONE)
		{
			if (images[i].elementCount == 3) format = GL_RGB;
			if (images[i].elementCount == 4) format = GL_RGBA;
		}
		else
		{
			if ((format == GL_RGB && images[i].elementCount == 4) ||
				(format == GL_RGBA && images[i].elementCount == 3))
			{
				std::string errorMessage = "Image files have different format";
				throw std::exception(errorMessage.c_str());
			}
		}

		if (images[i].width > largestWidth) largestWidth = images[i].width;
		if (images[i].height > largestHeight) largestHeight = images[i].height;
	}

	// Create texture
	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D_ARRAY, m_texture);

	// Set pixel storage mode
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// Specify texture
	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, format, largestWidth, largestHeight,
				 textureCount, 0, format, GL_UNSIGNED_BYTE, NULL);

	for (unsigned int i = 0; i < textureCount; i++)
	{
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, images[i].width, images[i].height,
						1, format, GL_UNSIGNED_BYTE, images[i].data);

		stbi_image_free(images[i].data);
	}

	// Default parameters
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
}

void GLTextureArray::Bind()
{
	glBindTexture(GL_TEXTURE_2D_ARRAY, m_texture);
}

void GLTextureArray::Bind(GLint index)
{
	// Select texture unit
	glActiveTexture(GL_TEXTURE0 + index);

	// Bind texture
	glBindTexture(GL_TEXTURE_2D_ARRAY, m_texture);
}