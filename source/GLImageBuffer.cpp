#include "GLImageBuffer.h"

void GLImageBuffer::Init(const void* data, GLsizeiptr dataSize, GLenum format)
{
	m_format = format;

	glGenBuffers(1, &m_buffer);
	glGenTextures(1, &m_texture);
	
	Resize(data, dataSize);
}

void GLImageBuffer::Resize(const void* data, GLsizeiptr dataSize)
{
	glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
	glBufferData(GL_ARRAY_BUFFER, dataSize, data, GL_DYNAMIC_COPY);

	glBindTexture(GL_TEXTURE_BUFFER, m_texture);
	glTexBuffer(GL_TEXTURE_BUFFER, m_format, m_buffer);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_BUFFER, 0);
}

void GLImageBuffer::Bind(GLuint location)
{
	glBindImageTexture(location, m_texture, 0, GL_FALSE, 0, GL_READ_WRITE, m_format);
}