#ifndef OCTREE_H
#define OCTREE_H

#include <glm/glm.hpp>
#include <algorithm>
#include <vector>

#include "MeshData.h"
#include "GLShaderBuffer.h"

class Octree
{
public:
	Octree();

	void AddVertices(std::vector<Vertex>* vertices, unsigned int minTrianglesPerNode,
					 unsigned int& baseNode, unsigned int& nodeCount);
	void Build();

	void Bind(GLuint location);

	void Reset();

private:
	struct AABB
	{
		glm::vec3 bottomLeftCorner,
				  topRightCorner,
				  center;

		AABB();
		AABB(glm::vec3 p0, glm::vec3 p1);
		AABB(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2);
		AABB(glm::vec4 p0, glm::vec4 p1);
		AABB(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2);

		void Union(AABB aabb);

		bool IsInside(glm::vec3 p);
		bool IsInside(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2);
		bool IsInside(glm::vec4 p);
		bool IsInside(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2);
	};

	struct BuildNode
	{
		AABB aabb;
		std::vector<Vertex> vertices;
		int children[8];
	};

	struct Node
	{
		unsigned int baseVertex, vertexCount;
		unsigned int childOffset;
		float padding;
		glm::vec4 bottomLeftCorner;
		glm::vec4 topRightCorner;
	};

private:
	void RecursiveBuild(int nodeIndex, std::vector<Vertex> vertices);
	void CreateBuildNodeAABBs(BuildNode* node, BuildNode children[8]);
	void AddTriangle(std::vector<Vertex>* vertices, Vertex v0, Vertex v1, Vertex v2);

	void RecursiveNodeCreation(unsigned int nodeIndex, unsigned int nodeOffset, unsigned int& nodeCount, std::vector<Vertex>* vertices);

private:
	GLShaderBuffer m_nodeBuffer;
	std::vector<Node> m_nodes;
	unsigned int m_vertexCount;

	// Used during build
	unsigned int m_minTrianglesPerNode;
	std::vector<BuildNode> m_buildNodes;
};

#endif