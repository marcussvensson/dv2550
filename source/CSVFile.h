#ifndef CSVFILE_H
#define CSVFILE_H

#include <fstream>
#include <string>

class CSVFile
{
public:
	~CSVFile();

	void Init(const std::string& filepath, bool append = true);
	void Write(const std::string& text);

private:
	std::fstream m_file;
};

#endif