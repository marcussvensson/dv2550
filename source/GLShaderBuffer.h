#ifndef GLSHADERBUFFER_H
#define GLSHADERBUFFER_H

#include <GL/glew.h>

class GLShaderBuffer
{
public:
	GLShaderBuffer();
	~GLShaderBuffer();

	void Init(GLsizeiptr dataSize, GLenum usage);
	void Resize(GLsizeiptr dataSize, GLenum usage);

	void* Map();
	void Unmap();

	void Bind(GLuint location);

private:
	GLuint m_buffer;
	GLsizeiptr m_dataSize;
	bool m_initialized;
};

#endif