#ifndef GLFRAMETEXTURE_H
#define GLFRAMETEXTURE_H

#include <GL/glew.h>

class GLFrameTexture
{
public:
	GLFrameTexture();

	bool Init(unsigned int attachmentIndex, int width, int height);
	void Resize(int width, int height);
	void Clear();
	void Bind(GLuint location);
	void Render(int width, int height);

private:
	GLuint m_framebuffer;
	GLuint m_framebufferTexture;

	GLenum m_attachment;
	int m_width,
		m_height;
};

#endif