#if defined(WIN32) && defined(_DEBUG)
#include <vld.h>
#endif

#include "GLRenderer.h"

int main(int argc, char* argv[])
{
	GLRenderer window;
	window.Init("DV2550", 1024, 768);
	window.Run();

	return 0;
}