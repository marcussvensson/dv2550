#ifndef MESH_H
#define MESH_H

#include <string>
#include <vector>
#include <glm/glm.hpp>

#include "MeshData.h"
#include "GLTextureArray.h"
#include "GLShaderBuffer.h"
#include "Octree.h"

class Mesh
{
public:
	void Build();

	void Reset();

	void BindVertexBuffer(GLuint location);
	void BindIndexBuffer(GLuint location);
	void BindMaterialBuffer(GLuint location);
	void BindTextureArray(GLuint location);
	void BindOctreeNodes(GLuint location);

	int Load(const std::string& filepath);
	int Load(MeshData meshData);

	void RecalculateData(int index, glm::mat4 model);
	void RecalculateData(int index, glm::mat4 model, unsigned int animationIndex, float animationTime);

	unsigned int GetVertexCount() const;

private:
	bool CalculateBoneDeformations(int index, std::vector<glm::mat4>* boneDeformations,
								   unsigned int animationIndex, float animationTime);

	void CalculateBoneDeformation(const MeshData::Node* node, std::vector<glm::mat4>* boneDeformations,
								  unsigned int animationIndex, float animationTime, glm::mat4 modelInverse,
								  const glm::mat4& parentDeformation = glm::mat4(1.f));

	glm::mat4 CalculateTranslationDeformation(const MeshData::Node::Animation* animation, float animationTime);
	glm::mat4 CalculateRotationDeformation(const MeshData::Node::Animation* animation, float animationTime);
	glm::mat4 CalculateScalingDeformation(const MeshData::Node::Animation* animation, float animationTime);

	unsigned int FindTranslationIndex(const MeshData::Node::Animation* animation, float animationTime);
	unsigned int FindRotationIndex(const MeshData::Node::Animation* animation, float animationTime);
	unsigned int FindScalingIndex(const MeshData::Node::Animation* animation, float animationTime);

private:
	std::vector<MeshData> m_originalDatas;
	std::vector<MeshData> m_calculatedDatas;
	
	GLShaderBuffer m_vertexBuffer;
	GLShaderBuffer m_indexBuffer;
	GLShaderBuffer m_materialBuffer;
	GLTextureArray m_textureArray;

	Octree m_octree;

	unsigned int m_vertexCount;
};

#endif