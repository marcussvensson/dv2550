#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>

class Camera
{
public:
	Camera();

	void Walk(float d);
	void Strafe(float d);

	void Pitch(float angle);
	void Yaw(float angle);

	void LookAt(glm::vec3 position, glm::vec3 target, glm::vec3 up = glm::vec3(0.f, 1.f, 0.f));

	void NormalizeVectors();

	glm::vec3 GetPosition() const;
	glm::vec3 GetRight() const;
	glm::vec3 GetUp() const;
	glm::vec3 GetLook() const;

private:
	glm::vec3 m_position;
	glm::vec3 m_right;
	glm::vec3 m_up;
	glm::vec3 m_look;
};

#endif