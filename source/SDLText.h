#ifndef SDLTEXT_H
#define SDLTEXT_H

#include <SDL.h>
#include <SDL_ttf.h>
#include <string>
#include <vector>

class SDLText
{
public:
	SDLText();
	~SDLText();

	bool Init(int width, int height);
	void Resize(int width, int height);

	int AddFont(const std::string& fontFilepath, int fontSize);

	void Render(int fontIndex, const std::string& textMessage, float x, float y, SDL_Color color);

private:
	bool m_initialized;
	int m_width,
		m_height;
	std::vector<TTF_Font*> m_fonts;
};

#endif
