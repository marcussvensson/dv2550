#include <iostream>

#include "Mesh.h"
#include "AssimpLoader.h"

void Mesh::Build()
{
	unsigned int totalVertexCount = 0;
	unsigned int totalIndexCount = 0;
	unsigned int totalMeshCount = 0;
	std::vector<std::string> textureFilepaths;
	for (MeshData meshData : m_calculatedDatas)
	{
		totalVertexCount += (unsigned int)meshData.positions.size();
		totalIndexCount += (unsigned int)meshData.indices.size();
		totalMeshCount += (unsigned int)meshData.ranges.size();
		textureFilepaths.insert(textureFilepaths.end(), meshData.textureFilepaths.begin(), meshData.textureFilepaths.end());
	}

	m_vertexBuffer.Init(totalVertexCount * 48, GL_STATIC_COPY);
	m_indexBuffer.Init(totalIndexCount * 4, GL_STATIC_COPY);
	m_materialBuffer.Init(totalMeshCount * 32, GL_STATIC_COPY);
	if (!textureFilepaths.empty())
		m_textureArray.Init(textureFilepaths);

	m_octree.Reset();

	std::vector<Vertex> vertexVector;
	std::vector<Material> materialVector(totalMeshCount);
	unsigned int vertexOffset = 0;
	unsigned int materialOffset = 0;
	unsigned int textureOffset = 0;
	for (MeshData meshData : m_calculatedDatas)
	{
		unsigned int meshCount = (unsigned int)meshData.ranges.size();
		for (unsigned int i = 0; i < meshCount; ++i)
		{
			unsigned int materialIndex = i + materialOffset;
			unsigned int baseVertex = meshData.ranges[i].startVertex;
			unsigned int vertexCount = meshData.ranges[i].vertexCount;
			std::vector<Vertex> v(vertexCount);
			for (unsigned int j = 0; j < vertexCount; ++j)
			{
				v[j].position = glm::vec4(meshData.positions[j + baseVertex], 1.0f);
				v[j].normal = glm::vec4(meshData.normals[j + baseVertex], 0.0f);
				v[j].texCoord = meshData.texCoords[j + baseVertex];
				//v[j].materialIndex = materialIndex;
			}

			unsigned int baseNode, nodeCount;
			m_octree.AddVertices(&v, 12, baseNode, nodeCount);

			vertexVector.insert(vertexVector.end(), v.begin(), v.end());

			materialVector[materialIndex].baseNode = baseNode;
			materialVector[materialIndex].nodeCount = nodeCount;
			materialVector[materialIndex].diffuseTextureIndex = meshData.materials[meshData.ranges[i].materialIndex].diffuseTextureIndex < 0 ? -1 : meshData.materials[meshData.ranges[i].materialIndex].diffuseTextureIndex + textureOffset;
			materialVector[materialIndex].normalMapTextureIndex = meshData.materials[meshData.ranges[i].materialIndex].normalMapTextureIndex < 0 ? -1 : meshData.materials[meshData.ranges[i].materialIndex].normalMapTextureIndex + textureOffset;
			materialVector[materialIndex].color = meshData.materials[meshData.ranges[i].materialIndex].color;
		}
		vertexOffset += (unsigned int)meshData.positions.size();
		materialOffset += meshCount;
		textureOffset += (unsigned int)meshData.textureFilepaths.size();
	}
	
	//m_octree.Build(&vertexVector, 25);
	m_octree.Build();

	// Build vertex buffer
	Vertex* vertices = (Vertex*)m_vertexBuffer.Map();
	memcpy(vertices, vertexVector.data(), totalVertexCount * 48);
	m_vertexBuffer.Unmap();

	// Build material buffer
	Material* materials = (Material*)m_materialBuffer.Map();
	memcpy(materials, materialVector.data(), totalMeshCount * 32);
	m_materialBuffer.Unmap();

	// Build index buffer
	/*unsigned int* indices = (unsigned int*)m_indexBuffer.Map();
	unsigned int indexOffset = 0;
	for (MeshData meshData : m_calculatedDatas)
	{
		unsigned int indexCount = (unsigned int)meshData.indices.size();
		for (unsigned int i = 0; i < indexCount; ++i)
		{
			unsigned int indexIndex = i + indexOffset;
			indices[indexIndex] = meshData.indices[i];
		}
		indexOffset += indexCount;
	}
	m_indexBuffer.Unmap();

	// Build mesh buffer
	Material* materials = (Material*)m_materialBuffer.Map();
	vertexOffset = 0;
	materialOffset = 0;
	unsigned int textureOffset = 0;
	for (MeshData meshData : m_calculatedDatas)
	{
		unsigned int meshCount = (unsigned int)meshData.ranges.size();
		for (unsigned int i = 0; i < meshCount; ++i)
		{
			unsigned int materialIndex = i + materialOffset;
			materials[materialIndex].diffuseTextureIndex = meshData.materials[meshData.ranges[i].materialIndex].diffuseTextureIndex < 0 ? -1 : meshData.materials[meshData.ranges[i].materialIndex].diffuseTextureIndex + textureOffset;
			materials[materialIndex].normalMapTextureIndex = meshData.materials[meshData.ranges[i].materialIndex].normalMapTextureIndex < 0 ? -1 : meshData.materials[meshData.ranges[i].materialIndex].normalMapTextureIndex + textureOffset;
			materials[materialIndex].color = meshData.materials[meshData.ranges[i].materialIndex].color;
		}
		vertexOffset += (unsigned int)meshData.positions.size();
		materialOffset += meshCount;
		textureOffset += (unsigned int)meshData.textureFilepaths.size();
	}
	m_materialBuffer.Unmap();*/

	m_vertexCount = totalVertexCount;
}

void Mesh::Reset()
{
	m_originalDatas.clear();
	m_calculatedDatas.clear();
}

void Mesh::BindVertexBuffer(GLuint location)
{
	m_vertexBuffer.Bind(location);
}

void Mesh::BindIndexBuffer(GLuint location)
{
	m_indexBuffer.Bind(location);
}

void Mesh::BindMaterialBuffer(GLuint location)
{
	m_materialBuffer.Bind(location);
}

void Mesh::BindTextureArray(GLuint location)
{
	m_textureArray.Bind(location);
}

void Mesh::BindOctreeNodes(GLuint location)
{
	m_octree.Bind(location);
}

int Mesh::Load(const std::string& filepath)
{
	MeshData meshData;
	try
	{
		AssimpLoader::GetInstance().Load(filepath, &meshData);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
		return -1;
	}

	assert(!meshData.positions.empty() &&
		   !meshData.normals.empty() &&
		   !meshData.texCoords.empty());

	m_originalDatas.push_back(meshData);
	m_calculatedDatas.push_back(meshData);

	RecalculateData((int)m_originalDatas.size() - 1, glm::mat4(1.0f));

	return (int)m_originalDatas.size() - 1;
}

int Mesh::Load(MeshData meshData)
{
	assert(!meshData.positions.empty() &&
		   !meshData.normals.empty() &&
		   !meshData.texCoords.empty());

	m_originalDatas.push_back(meshData);
	m_calculatedDatas.push_back(meshData);

	RecalculateData((int)m_originalDatas.size() - 1, glm::mat4(1.0f));

	return (int)m_originalDatas.size() - 1;
}

void Mesh::RecalculateData(int index, glm::mat4 model)
{
	// Make sure there's no weird behaviour
	if (index < 0 || index >= (int)m_originalDatas.size())
		return;

	glm::mat4 M = model * m_originalDatas[index].model;
	glm::mat4 tiM = glm::transpose(glm::inverse(M));

	for (unsigned int i = 0; i < (unsigned int)m_originalDatas[index].positions.size(); ++i)
	{
		m_calculatedDatas[index].positions[i] = glm::vec3(M * glm::vec4(m_originalDatas[index].positions[i], 1.0f));
		m_calculatedDatas[index].normals[i] = glm::vec3(tiM * glm::vec4(m_originalDatas[index].normals[i], 0.0f));
	}

	// Recalculate AABBs
	for (unsigned int i = 0; i < (unsigned int)m_originalDatas[index].ranges.size(); ++i)
	{
		m_calculatedDatas[index].ranges[i].minBoundingBox = glm::vec3(M * glm::vec4(m_originalDatas[index].ranges[i].minBoundingBox, 1.0f));
		m_calculatedDatas[index].ranges[i].maxBoundingBox = glm::vec3(M * glm::vec4(m_originalDatas[index].ranges[i].maxBoundingBox, 1.0f));
	}
}

void Mesh::RecalculateData(int index, glm::mat4 model, unsigned int animationIndex, float animationTime)
{
	// Make sure there's no weird behaviour
	if (index < 0 || index >= (int)m_originalDatas.size())
		return;

	std::vector<glm::mat4> boneDeformations;
	if (!CalculateBoneDeformations(index, &boneDeformations, animationIndex, animationTime))
		return;

	glm::mat4 M = model * m_originalDatas[index].model;
	glm::mat4 tiM = glm::transpose(glm::inverse(M));

	glm::vec4 boneWeight;
	glm::uvec4 boneIndexes;
	glm::mat4 boneTransformation;
	for (unsigned int i = 0; i < (unsigned int)m_originalDatas[index].positions.size(); ++i)
	{
		boneWeight = m_originalDatas[index].boneWeights[i];
		boneIndexes = m_originalDatas[index].boneIndexes[i];
		boneTransformation  = boneWeight[0] * boneDeformations[boneIndexes[0]];
		boneTransformation += boneWeight[1] * boneDeformations[boneIndexes[1]];
		boneTransformation += boneWeight[2] * boneDeformations[boneIndexes[2]];
		boneTransformation += boneWeight[3] * boneDeformations[boneIndexes[3]];

		m_calculatedDatas[index].positions[i] = glm::vec3(M * boneTransformation * glm::vec4(m_originalDatas[index].positions[i], 1.0f));
		m_calculatedDatas[index].normals[i] = glm::vec3(tiM * boneTransformation * glm::vec4(m_originalDatas[index].normals[i], 0.0f));
	}

	// Recalculate AABBs
	for (unsigned int i = 0; i < (unsigned int)m_originalDatas[index].ranges.size(); ++i)
	{
		glm::vec3* min = &m_calculatedDatas[index].ranges[i].minBoundingBox;
		glm::vec3* max = &m_calculatedDatas[index].ranges[i].maxBoundingBox;
		*min = glm::vec3(0.0f, 0.0f, 0.0f);
		*max = glm::vec3(0.0f, 0.0f, 0.0f);

		unsigned int startIndex = m_originalDatas[index].ranges[i].startIndex;
		unsigned int indexCount = m_originalDatas[index].ranges[i].startIndex +
								  m_originalDatas[index].ranges[i].indexCount;
		unsigned int baseVertex = m_originalDatas[index].ranges[i].startVertex;
		for (unsigned int j = startIndex; j < indexCount; j += 3)
		{
			unsigned int i0 = m_originalDatas[index].indices[j] + baseVertex;
			unsigned int i1 = m_originalDatas[index].indices[j + 1] + baseVertex;
			unsigned int i2 = m_originalDatas[index].indices[j + 2] + baseVertex;

			glm::vec3 p0 = m_originalDatas[index].positions[i0];
			glm::vec3 p1 = m_originalDatas[index].positions[i1];
			glm::vec3 p2 = m_originalDatas[index].positions[i2];

			if (p0.x < min->x) min->x = p0.x; if (p0.y < min->y) min->y = p0.y; if (p0.z < min->z) min->z = p0.z;
			if (p0.x > max->x) max->x = p0.x; if (p0.y > max->y) max->y = p0.y; if (p0.z > max->z) max->z = p0.z;
			if (p1.x < min->x) min->x = p1.x; if (p1.y < min->y) min->y = p1.y; if (p1.z < min->z) min->z = p1.z;
			if (p1.x > max->x) max->x = p1.x; if (p1.y > max->y) max->y = p1.y; if (p1.z > max->z) max->z = p1.z;
			if (p2.x < min->x) min->x = p2.x; if (p2.y < min->y) min->y = p2.y; if (p2.z < min->z) min->z = p2.z;
			if (p2.x > max->x) max->x = p2.x; if (p2.y > max->y) max->y = p2.y; if (p2.z > max->z) max->z = p2.z;
		}
	}
}

bool Mesh::CalculateBoneDeformations(int index, std::vector<glm::mat4>* boneDeformations,
									 unsigned int animationIndex, float animationTime)
{
	// Make sure there's no weird behaviour
	if (boneDeformations == NULL || boneDeformations == nullptr ||
		animationIndex >= unsigned int(m_originalDatas[index].animationDurations.size()) || animationTime < 0.0f)
		return false;

	boneDeformations->resize(m_originalDatas[index].boneCount);
	CalculateBoneDeformation(&m_originalDatas[index].root, boneDeformations, animationIndex, animationTime, m_originalDatas[index].modelInverse);

	return true;
}

void Mesh::CalculateBoneDeformation(const MeshData::Node* node, std::vector<glm::mat4>* boneDeformations,
									unsigned int animationIndex, float animationTime, glm::mat4 modelInverse,
									const glm::mat4& parentDeformation)
{
	glm::mat4 boneDeformation = node->transformation;
	if (node->animations[animationIndex].valid)
	{
		glm::mat4 T = CalculateTranslationDeformation(&node->animations[animationIndex], animationTime);
		glm::mat4 R = CalculateRotationDeformation(&node->animations[animationIndex], animationTime);
		glm::mat4 S = CalculateScalingDeformation(&node->animations[animationIndex], animationTime);
		boneDeformation = T * R * S;
	}
	glm::mat4 totalDeformation = parentDeformation * boneDeformation;
	if (node->hasBone)
	{
		const int boneIndex = node->index;
		boneDeformations->at(boneIndex) = modelInverse * totalDeformation * node->offset;
	}
	for (unsigned int i = 0; i < unsigned int(node->children.size()); ++i)
	{
		CalculateBoneDeformation(&node->children[i], boneDeformations, 
								 animationIndex, animationTime,
								 totalDeformation);
	}
}

glm::mat4 Mesh::CalculateTranslationDeformation(const MeshData::Node::Animation* animation, float animationTime)
{
	if (unsigned int(animation->translationKeys.size()) == 1)
	{
		return glm::translate(animation->translationKeys[0].value);
	}
	else
	{
		unsigned int translationIndex = FindTranslationIndex(animation, animationTime);
		unsigned int nextTranslationIndex = translationIndex + 1;

		float dt = animation->translationKeys[nextTranslationIndex].time - animation->translationKeys[translationIndex].time;
		float factor = (animationTime - animation->translationKeys[translationIndex].time) / dt;

		glm::vec3 start = animation->translationKeys[translationIndex].value;
		glm::vec3 end = animation->translationKeys[nextTranslationIndex].value;
		glm::vec3 d = end - start;
		return glm::translate(start + factor * d);
	}
}

glm::mat4 Mesh::CalculateRotationDeformation(const MeshData::Node::Animation* animation, float animationTime)
{
	if (unsigned int(animation->rotationKeys.size()) == 1)
	{
		return glm::mat4_cast(glm::normalize(animation->rotationKeys[0].value));
	}
	else
	{
		unsigned int rotationIndex = FindRotationIndex(animation, animationTime);
		unsigned int nextRotationIndex = rotationIndex + 1;

		float dt = animation->rotationKeys[nextRotationIndex].time - animation->rotationKeys[rotationIndex].time;
		float factor = (animationTime - animation->rotationKeys[rotationIndex].time) / dt;

		glm::quat start = animation->rotationKeys[rotationIndex].value;
		glm::quat end = animation->rotationKeys[nextRotationIndex].value;
		glm::quat rotation = glm::mix(start, end, factor);
		return glm::mat4_cast(glm::normalize(rotation));
	}
}

glm::mat4 Mesh::CalculateScalingDeformation(const MeshData::Node::Animation* animation, float animationTime)
{
	if (unsigned int(animation->scalingKeys.size()) == 1)
	{
		return glm::scale(animation->scalingKeys[0].value);
	}
	else
	{
		unsigned int scalingIndex = FindScalingIndex(animation, animationTime);
		unsigned int nextScalingIndex = scalingIndex + 1;

		float dt = animation->scalingKeys[nextScalingIndex].time - animation->scalingKeys[scalingIndex].time;
		float factor = (animationTime - animation->scalingKeys[scalingIndex].time) / dt;

		glm::vec3 start = animation->scalingKeys[scalingIndex].value;
		glm::vec3 end = animation->scalingKeys[nextScalingIndex].value;
		glm::vec3 d = end - start;
		return glm::scale(start + factor * d);
	}
}

#define FIND_INDEX(keys) \
	for (unsigned int i = 0; i < unsigned int(animation->keys.size()) - 1; i++) \
		if (animationTime < float(animation->keys[i + 1].time)) \
			return i; \
	return 0;

unsigned int Mesh::FindTranslationIndex(const MeshData::Node::Animation* animation, float animationTime)
{
	FIND_INDEX(translationKeys);
}

unsigned int Mesh::FindRotationIndex(const MeshData::Node::Animation* animation, float animationTime)
{
	FIND_INDEX(rotationKeys);
}

unsigned int Mesh::FindScalingIndex(const MeshData::Node::Animation* animation, float animationTime)
{
	FIND_INDEX(scalingKeys);
}

unsigned int Mesh::GetVertexCount() const
{
	return m_vertexCount;
}