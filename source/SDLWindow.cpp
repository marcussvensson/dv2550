#include "SDLWindow.h"
#include "Timer.h"

SDLWindow::SDLWindow()
{
	m_window = nullptr;
	m_context = nullptr;
	m_run = false;

	m_width = 0;
	m_height = 0;
}

bool SDLWindow::Init(const std::string& name, int width, int height)
{
	// Make sure there's no weird behaviour
	if (m_window || m_context || m_run)
		return false;

	// Initialize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		Quit();
		return false;
	}
	// Set attributes for the context
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	// Create window
	m_window = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
								width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if (!m_window)
	{
		Quit();
		return false;
	}
	// Create context
	m_context = SDL_GL_CreateContext(m_window);
	SDL_GL_MakeCurrent(m_window, m_context);

	// Set swap interval
	SDL_GL_SetSwapInterval(0);

	// Get initial cursor position
	SDL_GetMouseState(&m_previousCursorPositionX, &m_previousCursorPositionY);

	m_run = true;
	m_width = width;
	m_height = height;

	return true;
}

void SDLWindow::Run()
{
	SDL_Event e;
	Timer timer;

	while (m_run)
	{
		while (SDL_PollEvent(&e) != 0)
		{
			switch(e.type)
			{
			case SDL_WINDOWEVENT:
				if (e.window.event == SDL_WINDOWEVENT_RESIZED)
				{
					m_width = e.window.data1;
					m_height = e.window.data2;
					OnResize(m_width, m_height);
				}
				break;
			case SDL_MOUSEMOTION:
				int x, y;
				m_currentMouseState = SDL_GetMouseState(&x, &y);
				OnMouseMove(x - m_previousCursorPositionX,
							y - m_previousCursorPositionY);
				m_previousCursorPositionX = x;
				m_previousCursorPositionY = y;
				break;
			case SDL_KEYDOWN:
				OnKeyDown(e.key.keysym.sym);
				break;
			case SDL_QUIT:
				m_run = false;
				break;
			}
		}

		m_currentKeyboardState = SDL_GetKeyboardState(NULL);
		m_currentMouseState = SDL_GetMouseState(NULL, NULL);

		float dt = timer.ElapsedTimeInSeconds();
		timer.Reset();
		Update(dt);
		Render();

		SDL_GL_SwapWindow(m_window);
	}

	Quit();
}

void SDLWindow::SetSize(int width, int height)
{
	SDL_SetWindowSize(m_window, width, height);
	m_width = width;
	m_height = height;
	OnResize(m_width, m_height);
}

void SDLWindow::Quit()
{
	if (!m_context)
	{
		SDL_GL_DeleteContext(m_context);
		m_context = NULL;
	}
	if (!m_window)
	{
		SDL_DestroyWindow(m_window);
		m_window = NULL;
	}
	SDL_Quit();
}

bool SDLWindow::IsPressed(SDL_Scancode key) const
{
	return m_currentKeyboardState[key] != 0;
}

bool SDLWindow::IsPressed(unsigned int button) const
{
	return (m_currentMouseState & SDL_BUTTON(button)) != 0;
}

void SDLWindow::OnResize(int width, int height) { }

void SDLWindow::OnMouseMove(int dx, int dy) { }

void SDLWindow::OnKeyDown(SDL_Keycode key) { }