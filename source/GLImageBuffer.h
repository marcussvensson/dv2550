#ifndef GLIMAGEBUFFER_H
#define GLIMAGEBUFFER_H

#include <GL/glew.h>

class GLImageBuffer
{
public:
	void Init(const void* data, GLsizeiptr dataSize, GLenum format);
	void Resize(const void* data, GLsizeiptr dataSize);
	void Bind(GLuint location);

private:
	GLuint m_buffer;
	GLuint m_texture;
	GLenum m_format;
};

#endif