#ifndef MESHDATA_H
#define MESHDATA_H

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <vector>

struct Vertex
{
	glm::vec4 position;
	glm::vec4 normal;
	glm::vec2 texCoord;
	glm::vec2 padding;
};

struct Material
{
	unsigned int baseNode, nodeCount;
	int diffuseTextureIndex, normalMapTextureIndex;
	glm::vec4 color;
};

struct MeshData
{
	struct Range
	{
		unsigned int materialIndex;
		unsigned int vertexCount;
		unsigned int indexCount;
		unsigned int startVertex;
		unsigned int startIndex;

		bool useBoundingBox;
		glm::vec3 minBoundingBox, maxBoundingBox;
	};

	struct Material
	{
		int diffuseTextureIndex;
		int normalMapTextureIndex;
		glm::vec4 color; // If diffuse texture index equals -1, use color
	};

	struct Node
	{
		struct Animation
		{
			struct TranslationKey
			{
				float time;
				glm::vec3 value;
			};

			struct RotationKey
			{
				float time;
				glm::quat value;
			};

			struct ScalingKey
			{
				float time;
				glm::vec3 value;
			};

			std::vector<TranslationKey> translationKeys;
			std::vector<RotationKey> rotationKeys;
			std::vector<ScalingKey> scalingKeys;

			bool valid;
		};

		unsigned int index;

		glm::mat4 transformation;
		glm::mat4 offset;

		bool hasBone;

		std::vector<Animation> animations;

		std::vector<Node> children;
	};

	std::vector<glm::vec3> positions;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texCoords;
	std::vector<glm::uvec4> boneIndexes;
	std::vector<glm::vec4> boneWeights;

	std::vector<Range> ranges;
	std::vector<Material> materials;
	std::vector<std::string> textureFilepaths;

	std::vector<unsigned int> indices;

	Node root;
	unsigned int boneCount;
	std::vector<float> animationDurations;

	glm::mat4 model;
	glm::mat4 modelInverse;
};

#endif