#include <glm/gtc/type_ptr.hpp>
#include <fstream>
#include <vector>
#include <iostream>

#include "GLProgramHandler.h"

std::string ReadBinaryDataFromFile(const std::string& filepath)
{
	// Open file
	std::ifstream file(filepath, std::ifstream::binary);
	if (!file)
	{
		std::string errorMessage = "Unable to open file \"" + filepath + "\"";
		throw std::exception(errorMessage.c_str());
	}

	// Get file length
	file.seekg(0, file.end);
	const unsigned int length = (unsigned int)file.tellg();
	file.seekg(0, file.beg);

	// Read file
	std::vector<char> data(length);
	file.read((char*)data.data(), (std::streamsize)length);

	// Close file
	file.close();

	// Null-terminate data
	data.push_back(NULL);

	// Return as string
	return std::string(const_cast<const char*>(data.data()));
}

GLProgramHandler::GLProgramHandler() { }

GLProgramHandler::~GLProgramHandler()
{
	for (auto program : m_programs)
	{
		glDeleteProgram(program.second.first);
	}
}

void GLProgramHandler::AddFromFile(const std::string& programName, const ShaderFile* shaderFiles, unsigned int shaderFileCount)
{
	assert(shaderFiles != NULL && shaderFileCount > 0);

	std::vector<ShaderSource> shaderSources(shaderFileCount);
	for (unsigned int i = 0; i < shaderFileCount; i++)
	{
		shaderSources[i].source = ReadBinaryDataFromFile(shaderFiles[i].filepath);
		shaderSources[i].type = shaderFiles[i].type;
	}

	AddFromSource(programName, shaderSources.data(), (unsigned int)shaderSources.size());
}

void GLProgramHandler::AddFromSource(const std::string& programName, const ShaderSource* shaderSources, unsigned int shaderSourceCount)
{
	assert(m_programs.find(programName) == m_programs.end());

	assert(shaderSources != NULL && shaderSourceCount > 0);

	GLuint program = glCreateProgram();
	assert(program != 0);

	std::vector<GLuint> shaders(shaderSourceCount);
	for (unsigned int i = 0; i < shaderSourceCount; i++)
	{
		shaders[i] = glCreateShader(shaderSources[i].type);
		assert(shaders[i] != 0);

		const char* source = shaderSources[i].source.c_str();
		assert(source != NULL);
		glShaderSource(shaders[i], 1, &source, NULL);

		glCompileShader(shaders[i]);
		GLint compiled = GL_FALSE;
		glGetShaderiv(shaders[i], GL_COMPILE_STATUS, &compiled);
		if (compiled == GL_FALSE)
		{
			GLsizei length;
			glGetShaderiv(shaders[i], GL_INFO_LOG_LENGTH, &length);
			std::vector<GLchar> log(length + 1);
			glGetShaderInfoLog(shaders[i], length, &length, log.data());
			std::string errorMessage = "Parse error (" + programName + "):\n" + log.data();
			throw std::exception(errorMessage.c_str());
		}

		glAttachShader(program, shaders[i]);
	}

	glLinkProgram(program);
	GLint linked = GL_FALSE;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE)
	{
		GLsizei length;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
		std::vector<GLchar> log(length + 1);
		glGetProgramInfoLog(program, length, &length, log.data());
		std::string errorMessage = "Parse error (" + programName + "):\n" + log.data();
		throw std::exception(errorMessage.c_str());
	}

	for (unsigned int i = 0; i < shaderSourceCount; i++)
	{
		glDetachShader(program, shaders[i]);
		glDeleteShader(shaders[i]);
	}

	m_programs[programName] = std::pair<GLuint, int>(program, -1);
}

void GLProgramHandler::PrefetchUniforms(const std::string& programName, const std::string* uniformNames, unsigned int uniformNameCount)
{
	if (m_programs.find(programName) == m_programs.end())
		return;
	GLuint program = m_programs[programName].first;
	for (unsigned int i = 0; i < uniformNameCount; i++)
		m_programUniformNameLocations[program][uniformNames[i]] = glGetUniformLocation(program, uniformNames[i].c_str());
}

GLint GLProgramHandler::GetUniformLocation(const std::string& name)
{
	std::map<std::string, GLint>::iterator it = m_programUniformNameLocations[m_currentProgram].find(name);
	if (it != m_programUniformNameLocations[m_currentProgram].end())
		return it->second;
	return glGetUniformLocation(m_currentProgram, name.c_str());
}

void GLProgramHandler::Use(const std::string& programName)
{
	if (m_programs.find(programName) != m_programs.end())
	{
		m_currentProgram = m_programs[programName].first;
		glUseProgram(m_currentProgram);
	}
}

void GLProgramHandler::SetUniform(const std::string& name, int value)
{
	glUniform1i(GetUniformLocation(name), value);
}

void GLProgramHandler::SetUniform(const std::string& name, unsigned int value)
{
	glUniform1ui(GetUniformLocation(name), value);
}

void GLProgramHandler::SetUniform(const std::string& name, float value)
{
	glUniform1f(GetUniformLocation(name), value);
}

void GLProgramHandler::SetUniform(const std::string& name, glm::vec2 value)
{
	glUniform2f(GetUniformLocation(name), value.x, value.y);
}

void GLProgramHandler::SetUniform(const std::string& name, glm::vec3 value)
{
	glUniform3f(GetUniformLocation(name), value.x, value.y, value.z);
}

void GLProgramHandler::SetUniform(const std::string& name, glm::vec4 value)
{
	glUniform4f(GetUniformLocation(name), value.x, value.y, value.z, value.w);
}

void GLProgramHandler::SetUniform(const std::string& name, glm::mat2 value)
{
	glUniformMatrix2fv(GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(value));
}

void GLProgramHandler::SetUniform(const std::string& name, glm::mat3 value)
{
	glUniformMatrix3fv(GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(value));
}

void GLProgramHandler::SetUniform(const std::string& name, glm::mat4 value)
{
	glUniformMatrix4fv(GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(value));
}

void GLProgramHandler::SetUniform(const std::string& name, const int* value, unsigned int count)
{
	assert(value != NULL && count > 0);
	GLint location = GetUniformLocation(name);
	for (unsigned int i = 0; i < count; i++)
	{
		glUniform1i(location + i, value[i]);
	}
}

void GLProgramHandler::SetUniform(const std::string& name, const unsigned int* value, unsigned int count)
{
	assert(value != NULL && count > 0);
	GLint location = GetUniformLocation(name);
	for (unsigned int i = 0; i < count; i++)
	{
		glUniform1ui(location + i, value[i]);
	}
}

void GLProgramHandler::SetUniform(const std::string& name, const float* value, unsigned int count)
{
	assert(value != NULL && count > 0);
	GLint location = GetUniformLocation(name);
	for (unsigned int i = 0; i < count; i++)
	{
		glUniform1f(location + i, value[i]);
	}
}

void GLProgramHandler::SetUniform(const std::string& name, const glm::vec2* value, unsigned int count)
{
	assert(value != NULL && count > 0);
	GLint location = GetUniformLocation(name);
	for (unsigned int i = 0; i < count; i++)
	{
		glUniform2f(location + i, value[i].x, value[i].y);
	}
}

void GLProgramHandler::SetUniform(const std::string& name, const glm::vec3* value, unsigned int count)
{
	assert(value != NULL && count > 0);
	GLint location = GetUniformLocation(name);
	for (unsigned int i = 0; i < count; i++)
	{
		glUniform3f(location + i, value[i].x, value[i].y, value[i].z);
	}
}

void GLProgramHandler::SetUniform(const std::string& name, const glm::vec4* value, unsigned int count)
{
	assert(value != NULL && count > 0);
	GLint location = GetUniformLocation(name);
	for (unsigned int i = 0; i < count; i++)
	{
		glUniform4f(location + i, value[i].x, value[i].y, value[i].z, value[i].w);
	}
}

void GLProgramHandler::SetUniform(const std::string& name, const glm::mat2* value, unsigned int count)
{
	assert(value != NULL && count > 0);
	GLint location = GetUniformLocation(name);
	for (unsigned int i = 0; i < count; i++)
	{
		glUniformMatrix2fv(location + i, 1, GL_FALSE, glm::value_ptr(value[i]));
	}
}

void GLProgramHandler::SetUniform(const std::string& name, const glm::mat3* value, unsigned int count)
{
	assert(value != NULL && count > 0);
	GLint location = GetUniformLocation(name);
	for (unsigned int i = 0; i < count; i++)
	{
		glUniformMatrix3fv(location + i, 1, GL_FALSE, glm::value_ptr(value[i]));
	}
}

void GLProgramHandler::SetUniform(const std::string& name, const glm::mat4* value, unsigned int count)
{
	assert(value != NULL && count > 0);
	GLint location = GetUniformLocation(name);
	for (unsigned int i = 0; i < count; i++)
	{
		glUniformMatrix4fv(location + i, 1, GL_FALSE, glm::value_ptr(value[i]));
	}
}