#include <glm/ext.hpp>

#include "Camera.h"

Camera::Camera()
{
	m_position = glm::vec3(0.0f, 0.0f,  0.0f);
	m_right	   = glm::vec3(1.0f, 0.0f,  0.0f);
	m_up	   = glm::vec3(0.0f, 1.0f,  0.0f);
	m_look	   = glm::vec3(0.0f, 0.0f, -1.0f);
}

void Camera::Walk(float d)
{
	m_position += d * m_look;
}

void Camera::Strafe(float d)
{
	m_position += d * m_right;
}

void Camera::Pitch(float angle)
{
	glm::mat4 R = glm::rotate(angle, m_right);
	m_up = glm::vec3(R * glm::vec4(m_up, 0.f));
	m_look = glm::vec3(R * glm::vec4(m_look, 0.f));
}

void Camera::Yaw(float angle)
{
	glm::mat4 R = glm::rotate(angle, glm::vec3(0.f, 1.f, 0.f));
	m_right = glm::vec3(R * glm::vec4(m_right, 0.f));
	m_up = glm::vec3(R * glm::vec4(m_up, 0.f));
	m_look = glm::vec3(R * glm::vec4(m_look, 0.f));
}

void Camera::LookAt(glm::vec3 position, glm::vec3 target, glm::vec3 up)
{
	m_position = position;
	m_look = glm::normalize(target - position);
	m_right = glm::normalize(glm::cross(m_look, up));
	m_up = glm::cross(m_right, m_look);
}

void Camera::NormalizeVectors()
{
	m_look = glm::normalize(m_look);
	m_up = glm::normalize(glm::cross(m_right, m_look));
	m_right = glm::cross(m_look, m_up);
}

glm::vec3 Camera::GetPosition() const
{
	return m_position;
}

glm::vec3 Camera::GetRight() const
{
	return m_right;
}

glm::vec3 Camera::GetUp() const
{
	return m_up;
}

glm::vec3 Camera::GetLook() const
{
	return m_look;
}