#include <GL\glew.h>
#include <iostream>

#include "SDLText.h"

SDLText::SDLText()
{
	m_initialized = false;
}

SDLText::~SDLText()
{
	for (TTF_Font* font : m_fonts)
		TTF_CloseFont(font);

	if (m_initialized)
		TTF_Quit();
}

bool SDLText::Init(int width, int height)
{
	if (TTF_Init() < 0)
		return false;

	m_initialized = true;
	Resize(width, height);

	return true;
}

void SDLText::Resize(int width, int height)
{
	m_width = width;
	m_height = height;
}

int SDLText::AddFont(const std::string& fontFilepath, int fontSize)
{
	if (!m_initialized)
		return -1;

	TTF_Font* font = TTF_OpenFont(fontFilepath.c_str(), fontSize);
	if (!font)
		return -1;
	m_fonts.push_back(font);
	return m_fonts.size() - 1;
}

void SDLText::Render(int fontIndex, const std::string& textMessage, float x, float y, SDL_Color color)
{
	// Make sure there's no weird behaviour
	if (!m_initialized || fontIndex < 0 || fontIndex >= (int)m_fonts.size())
		return;

	// Create SDL surface
	SDL_Surface* text = TTF_RenderText_Blended_Wrapped(m_fonts[fontIndex], textMessage.c_str(), color, (Uint32)500/*(m_width - x - x)*/);

	// Create OpenGL texture
	GLuint texture;
	glGenTextures(1, &texture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, text->w, text->h, 0, GL_BGRA,
				 GL_UNSIGNED_BYTE, text->pixels);

	// Setup rendering properties
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Push matrices
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	// Draw texture quad
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(2.0f * x / (float)m_width - 1.0f, 2.0f * (1.0f - y / (float)m_height) - 1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(2.0f * (x + (float)text->w) / (float)m_width - 1.0f, 2.0f * (1.0f - y / (float)m_height) - 1.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(2.0f * (x + (float)text->w) / (float)m_width - 1.0f, 2.0f * (1.0f - (y + (float)text->h) / (float)m_height) - 1.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(2.0f * x / (float)m_width - 1.0f, 2.0f * (1.0f -(y + (float)text->h) / (float)m_height) - 1.0f, 0.0f);
	glEnd();

	// Pop matrices
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	// Deallocate resources
	glDeleteTextures(1, &texture);
	SDL_FreeSurface(text);
}
