#include "GLFrameTexture.h"

GLFrameTexture::GLFrameTexture()
{
	m_width = 0;
	m_height = 0;
}

bool GLFrameTexture::Init(unsigned int attachmentIndex, int width, int height)
{
	if (attachmentIndex > 16)
		return false;

	m_attachment = GL_COLOR_ATTACHMENT0 + attachmentIndex;

	glGenFramebuffers(1, &m_framebuffer);
	glGenTextures(1, &m_framebufferTexture);

	Resize(width, height);

	glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);
	glBindTexture(GL_TEXTURE_2D, m_framebufferTexture);
	glFramebufferTexture2D(GL_FRAMEBUFFER, m_attachment, GL_TEXTURE_2D, m_framebufferTexture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return true;
}

void GLFrameTexture::Resize(int width, int height)
{
	glBindTexture(GL_TEXTURE_2D, m_framebufferTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	m_width = width;
	m_height = height;
}

void GLFrameTexture::Clear()
{
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_framebuffer);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glClear(GL_COLOR_BUFFER_BIT);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void GLFrameTexture::Bind(GLuint location)
{
	glBindImageTexture(location, m_framebufferTexture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
}

void GLFrameTexture::Render(int width, int height)
{
	glBindFramebuffer(GL_READ_FRAMEBUFFER, m_framebuffer);
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glBlitFramebuffer(0, 0, m_width, m_height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_LINEAR);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}
