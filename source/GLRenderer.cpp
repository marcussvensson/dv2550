#include <GL/glew.h>
#include <iostream>
#include <glm/glm.hpp>

#include "GLRenderer.h"
#include "Timer.h"

bool GLRenderer::Init(const std::string& name, int width, int height)
{
	if (!SDLWindow::Init(name, width, height))
		return false;

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
		return false;

	if (!InitPrograms())
		return false;

	m_camera.LookAt(glm::vec3(-3.0f, -2.0f, -3.0f), glm::vec3(0.0f, 0.0f, 0.0f));

	m_frameTexture.Init(0, width, height);

	const int bufferSize = width * height;
	m_rayBuffer.Init(bufferSize * 60, GL_DYNAMIC_COPY);
	m_intersectionBuffer.Init(bufferSize * 32, GL_DYNAMIC_COPY);

	m_bounceCount = 0;
	m_pointLightCount = 1;
	m_useSupersampling = false;

	CreatePointLights();
	CreateDirectionalLights();

	m_text.Init(width, height);
	m_text.AddFont("../assets/verdana.ttf", 16);

	m_modelMeshIndex = 0;
	m_modelMeshInfos.push_back({ "../assets/uzi.3ds", glm::vec3(0.0f, 0.0f, 0.0f), 0.1f });
	m_modelMeshInfos.push_back({ "../assets/turtle1.ms3d", glm::vec3(0.0f, -0.5f, 0.0f), 0.1f });
	m_modelMeshInfos.push_back({ "../assets/dwarf1.ms3d", glm::vec3(0.0f, -2.0f, 0.0f), 0.05f });
	//m_modelMeshInfos.push_back({ "../assets/box.obj", glm::vec3(0.0f, 0.0f, 0.0f), 1.0f });
	CreateModelRoomMesh(m_modelMeshInfos[m_modelMeshIndex]);

	m_frameIndex = 0;
	m_testIndex = 0;
	//InitTest();

	return true;
}

void GLRenderer::Update(float dt)
{
	const float movementSpeed = 25.0f;
	if (IsPressed(SDL_SCANCODE_W))
		m_camera.Walk(dt * movementSpeed);
	if (IsPressed(SDL_SCANCODE_S))
		m_camera.Walk(dt * -movementSpeed);
	if (IsPressed(SDL_SCANCODE_A))
		m_camera.Strafe(dt * -movementSpeed);
	if (IsPressed(SDL_SCANCODE_D))
		m_camera.Strafe(dt * movementSpeed);
	m_camera.NormalizeVectors();

	m_currentFPS = 1.0f / dt;
}

void GLRenderer::Render()
{
	//TickTest();

	const unsigned int localSizeX = 16;
	const unsigned int localSizeY = 16;
	const unsigned int numGroupsX = (unsigned int)ceil((float)(m_useSupersampling ? 2 * m_width : m_width) / (float)localSizeX);
	const unsigned int numGroupsY = (unsigned int)ceil((float)(m_useSupersampling ? 2 * m_height : m_height) / (float)localSizeY);

	glClear(GL_COLOR_BUFFER_BIT);

	m_frameTexture.Clear();

	std::string textMessage;
	Timer timer;
	float primaryRayGenerationTime = 0.0f,
		  intersectionStageTime = 0.0f,
		  colorStageTime = 0.0f;

	int width = m_width;
	int height = m_height;
	if (m_useSupersampling)
	{
		width *= 2;
		height *= 2;
	}

	// PRIMARY RAY GENERATION
	m_programHandler.Use("PrimaryRayGeneration");
	m_rayBuffer.Bind(0);
	m_programHandler.SetUniform("uClientWidth", width);
	m_programHandler.SetUniform("uClientHeight", height);
	m_programHandler.SetUniform("uCamera.position", m_camera.GetPosition());
	m_programHandler.SetUniform("uCamera.right", m_camera.GetRight());
	m_programHandler.SetUniform("uCamera.up", m_camera.GetUp());
	m_programHandler.SetUniform("uCamera.look", m_camera.GetLook());
	m_programHandler.SetUniform("uCamera.fov", 1.309f); // 75 degrees FOV

	glDispatchCompute(numGroupsX, numGroupsY, 1);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	for (unsigned int i = 0; i < m_bounceCount + 1; i++)
	{
		// INTERSECTION STAGE
		m_programHandler.Use("IntersectionStage");
		m_rayBuffer.Bind(0);
		m_intersectionBuffer.Bind(1);
		m_mesh.BindVertexBuffer(2);
		m_mesh.BindMaterialBuffer(3);
		m_mesh.BindOctreeNodes(4);
		m_programHandler.SetUniform("uClientWidth", width);
		m_programHandler.SetUniform("uClientHeight", height);

		glDispatchCompute(numGroupsX, numGroupsY, 1);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

		// COLOR STAGE
		m_programHandler.Use("ColorStage");
		m_rayBuffer.Bind(0);
		m_intersectionBuffer.Bind(1);
		m_mesh.BindVertexBuffer(2);
		m_mesh.BindMaterialBuffer(3);
		m_mesh.BindOctreeNodes(4);
		m_pointLightBuffer.Bind(5);
		m_directionalLightBuffer.Bind(6);
		m_frameTexture.Bind(7);
		m_mesh.BindTextureArray(8);
		m_programHandler.SetUniform("uTextures", 8);
		m_programHandler.SetUniform("uClientWidth", width);
		m_programHandler.SetUniform("uClientHeight", height);
		m_programHandler.SetUniform("uPointLightCount", m_pointLightCount);

		glDispatchCompute(numGroupsX, numGroupsY, 1);
		glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
	}

	m_frameTexture.Render(m_width, m_height);

	textMessage += "FPS: " + std::to_string(m_currentFPS) + "\n";
	textMessage += "Resolution: " + std::to_string(m_width) + " x " + std::to_string(m_height) + " pixels\n";
	textMessage += "Bounces: " + std::to_string(m_bounceCount) + "\n";
	textMessage += "Vertex count: " + std::to_string(m_mesh.GetVertexCount()) + "\n";
	textMessage += "Supersampling: ";
	if (m_useSupersampling)
		textMessage += "On\n\n";
	else
		textMessage += "Off\n\n";

	//textMessage += "Primary ray generation: " + std::to_string(primaryRayGenerationTime) + " ms\n";
	//textMessage += "Intersection stage: "+ std::to_string(intersectionStageTime) + " ms\n";
	//textMessage += "Color stage: " + std::to_string(colorStageTime) + " ms";

	SDL_Color white = { 255, 255, 255, 0 };
	m_text.Render(0, textMessage, 10.0f, 10.0f, white);

	//CollectTestData(primaryRayGenerationTime, intersectionStageTime, colorStageTime);
}

void GLRenderer::OnResize(int width, int height)
{
	int w = width;
	int h = height;

	glViewport(0, 0, w, h);
	m_text.Resize(w, h);
	
	if (m_useSupersampling)
	{
		w *= 2;
		h *= 2;
	}

	m_frameTexture.Resize(w, h);

	const int bufferSize = w * h;
	m_rayBuffer.Resize(bufferSize * 60, GL_STATIC_COPY);
	m_intersectionBuffer.Resize(bufferSize * 32, GL_STATIC_COPY);
}

void GLRenderer::OnMouseMove(int dx, int dy)
{
	if (IsPressed(SDL_BUTTON_RIGHT))
	{
		const float rotationSpeed = 0.5f;
		m_camera.Yaw(dx * -rotationSpeed);
		m_camera.Pitch(dy * -rotationSpeed);
	}
}

void GLRenderer::OnKeyDown(SDL_Keycode key)
{
	#define SET_POINT_LIGHT_COUNT(n) \
		case SDLK_1 + n - 1: \
			m_pointLightCount = n; \
			CreatePointLights(); \
			break;

	switch (key)
	{
	case SDLK_UP:
		m_bounceCount++;
		break;
	case SDLK_DOWN:
		if (m_bounceCount > 0)
			m_bounceCount--;
		break;
	case SDLK_RIGHT:
		m_modelMeshIndex = m_modelMeshIndex < (unsigned int)m_modelMeshInfos.size() - 1 ? m_modelMeshIndex + 1 : 0;
		CreateModelRoomMesh(m_modelMeshInfos[m_modelMeshIndex]);
		break;
	case SDLK_LEFT:
		m_modelMeshIndex = m_modelMeshIndex > 0 ? m_modelMeshIndex - 1 : (unsigned int)m_modelMeshInfos.size() - 1;
		CreateModelRoomMesh(m_modelMeshInfos[m_modelMeshIndex]);
		break;
	case SDLK_e:
		m_useSupersampling = !m_useSupersampling;
		OnResize(m_width, m_height);
		break;
	SET_POINT_LIGHT_COUNT(1);
	SET_POINT_LIGHT_COUNT(2);
	SET_POINT_LIGHT_COUNT(3);
	SET_POINT_LIGHT_COUNT(4);
	SET_POINT_LIGHT_COUNT(5);
	SET_POINT_LIGHT_COUNT(6);
	SET_POINT_LIGHT_COUNT(7);
	SET_POINT_LIGHT_COUNT(8);
	}
}

bool GLRenderer::InitPrograms()
{
	try
	{
		GLProgramHandler::ShaderFile shaderFile;
		shaderFile = { "../shaders/PrimaryRayGeneration.cs.glsl", GL_COMPUTE_SHADER };
		m_programHandler.AddFromFile("PrimaryRayGeneration", &shaderFile, 1);
		shaderFile = { "../shaders/IntersectionStage.cs.glsl", GL_COMPUTE_SHADER };
		m_programHandler.AddFromFile("IntersectionStage", &shaderFile, 1);
		shaderFile = { "../shaders/ColorStage.cs.glsl", GL_COMPUTE_SHADER };
		m_programHandler.AddFromFile("ColorStage", &shaderFile, 1);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
		return false;
	}

	const std::string primaryRayGenerationUniformNames[] =
	{
		"uClientWidth", "uClientHeight", "uCamera.position", "uCamera.right",
		"uCamera.up", "uCamera.look", "uCamera.fov"
	};
	const std::string intersectionStageUniformNames[] =
	{
		"uClientWidth", "uClientHeight"
	};
	const std::string colorStageUniformNames[] =
	{
		"uClientWidth", "uClientHeight", "uTextures", "uPointLightCount"
	};
	m_programHandler.PrefetchUniforms("PrimaryRayGeneration", primaryRayGenerationUniformNames,
		sizeof(primaryRayGenerationUniformNames) / sizeof(*primaryRayGenerationUniformNames));
	m_programHandler.PrefetchUniforms("IntersectionStage", intersectionStageUniformNames,
		sizeof(intersectionStageUniformNames) / sizeof(*intersectionStageUniformNames));
	m_programHandler.PrefetchUniforms("ColorStage", colorStageUniformNames,
		sizeof(colorStageUniformNames) / sizeof(*colorStageUniformNames));

	return true;
}

void GLRenderer::CreatePointLights()
{
	struct PointLight
	{
		glm::vec4 ambient;
		glm::vec4 diffuse;
		glm::vec4 specular;

		glm::vec3 position;
		float range;

		glm::vec4 attenuation;
	};

	const unsigned int pointLightCount = 8;
	m_pointLightBuffer.Init(pointLightCount * sizeof(PointLight), GL_STATIC_COPY);

	PointLight pl;
	pl.ambient = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	pl.diffuse = glm::vec4(0.6f, 0.6f, 0.6f, 1.0f);
	pl.specular = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);
	pl.range = 25.0f;
	pl.attenuation = glm::vec4(0.1f, 0.1f, 0.1f, 0.0f);
	glm::vec3 plPositions[] =
	{
		//glm::vec3(0.0f, 3.0f, 0.0f),

		glm::vec3(-2.5f, -2.5f, -2.5f),
		glm::vec3(-2.5f, -2.5f,  2.5f),
		glm::vec3(-2.5f,  2.5f, -2.5f),
		glm::vec3(-2.5f,  2.5f,  2.5f),
		glm::vec3( 2.5f, -2.5f, -2.5f),
		glm::vec3( 2.5f, -2.5f,  2.5f),
		glm::vec3( 2.5f,  2.5f, -2.5f),
		glm::vec3( 2.5f,  2.5f,  2.5f)
	};

	PointLight* pointLights = (PointLight*)m_pointLightBuffer.Map();
	for (unsigned int i = 0; i < pointLightCount; i++)
	{
		pl.position = plPositions[i];
		pointLights[i] = pl;
	}
	m_pointLightBuffer.Unmap();
}

void GLRenderer::CreateDirectionalLights()
{
	struct DirectionalLight
	{
		glm::vec4 ambient;
		glm::vec4 diffuse;
		glm::vec4 specular;

		glm::vec4 direction;
	};

	const unsigned int directionalLightCount = 1;
	m_directionalLightBuffer.Init(directionalLightCount * sizeof(DirectionalLight), GL_STATIC_COPY);

	DirectionalLight dl;
	dl.ambient = glm::vec4(0.1f, 0.1f, 0.1f, 1.0f);
	dl.diffuse = glm::vec4(0.1f, 0.1f, 0.1f, 1.0f);
	dl.specular = glm::vec4(0.05f, 0.05f, 0.05f, 1.0f);
	dl.direction = glm::normalize(glm::vec4(-1.0f, -1.0f, 1.0f, 0.0f));

	DirectionalLight* directionalLights = (DirectionalLight*)m_directionalLightBuffer.Map();
	for (unsigned int i = 0; i < directionalLightCount; i++)
		directionalLights[i] = dl;
	m_directionalLightBuffer.Unmap();
}

void GLRenderer::CreateModelRoomMesh(ModelMeshInfo modelMeshInfo)
{
	m_mesh.Reset();
	int index = m_mesh.Load(modelMeshInfo.filepath);
	m_mesh.RecalculateData(index, glm::translate(modelMeshInfo.position) * glm::mat4(glm::mat3(modelMeshInfo.scale)));
	CreateRoomMesh();
	m_mesh.Build();
}

void GLRenderer::CreateRoomMesh()
{
	MeshData meshData;
	meshData.positions.resize(36);
	meshData.normals.resize(36);
	meshData.texCoords.resize(36);
	meshData.indices.resize(36);

	// Positions
	// Floor
	meshData.positions[0] = glm::vec3(-1.0f, -1.0f, -1.0f);
	meshData.positions[1] = glm::vec3(-1.0f, -1.0f, 1.0f);
	meshData.positions[2] = glm::vec3(1.0f, -1.0f, -1.0f);
	meshData.positions[3] = glm::vec3(1.0f, -1.0f, 1.0f);
	meshData.positions[4] = glm::vec3(-1.0f, -1.0f, 1.0f);
	meshData.positions[5] = glm::vec3(1.0f, -1.0f, -1.0f);
	// Ceiling
	meshData.positions[6] = glm::vec3(-1.0f, 1.0f, -1.0f);
	meshData.positions[7] = glm::vec3(-1.0f, 1.0f, 1.0f);
	meshData.positions[8] = glm::vec3(1.0f, 1.0f, -1.0f);
	meshData.positions[9] = glm::vec3(1.0f, 1.0f, 1.0f);
	meshData.positions[10] = glm::vec3(-1.0f, 1.0f, 1.0f);
	meshData.positions[11] = glm::vec3(1.0f, 1.0f, -1.0f);
	// Left wall
	meshData.positions[12] = glm::vec3(-1.0f, -1.0f, -1.0f);
	meshData.positions[13] = glm::vec3(-1.0f, 1.0f, -1.0f);
	meshData.positions[14] = glm::vec3(-1.0f, -1.0f, 1.0f);
	meshData.positions[15] = glm::vec3(-1.0f, 1.0f, 1.0f);
	meshData.positions[16] = glm::vec3(-1.0f, 1.0f, -1.0f);
	meshData.positions[17] = glm::vec3(-1.0f, -1.0f, 1.0f);
	// Right wall
	meshData.positions[18] = glm::vec3(1.0f, -1.0f, -1.0f);
	meshData.positions[19] = glm::vec3(1.0f, 1.0f, -1.0f);
	meshData.positions[20] = glm::vec3(1.0f, -1.0f, 1.0f);
	meshData.positions[21] = glm::vec3(1.0f, 1.0f, 1.0f);
	meshData.positions[22] = glm::vec3(1.0f, 1.0f, -1.0f);
	meshData.positions[23] = glm::vec3(1.0f, -1.0f, 1.0f);
	// Near wall
	meshData.positions[24] = glm::vec3(-1.0f, -1.0f, 1.0f);
	meshData.positions[25] = glm::vec3(-1.0f, 1.0f, 1.0f);
	meshData.positions[26] = glm::vec3(1.0f, -1.0f, 1.0f);
	meshData.positions[27] = glm::vec3(1.0f, 1.0f, 1.0f);
	meshData.positions[28] = glm::vec3(-1.0f, 1.0f, 1.0f);
	meshData.positions[29] = glm::vec3(1.0f, -1.0f, 1.0f);
	// Far wall
	meshData.positions[30] = glm::vec3(-1.0f, -1.0f, -1.0f);
	meshData.positions[31] = glm::vec3(-1.0f, 1.0f, -1.0f);
	meshData.positions[32] = glm::vec3(1.0f, -1.0f, -1.0f);
	meshData.positions[33] = glm::vec3(1.0f, 1.0f, -1.0f);
	meshData.positions[34] = glm::vec3(-1.0f, 1.0f, -1.0f);
	meshData.positions[35] = glm::vec3(1.0f, -1.0f, -1.0f);

	// Normals
	// Floor
	meshData.normals[0] = meshData.normals[1] = meshData.normals[2] =
		meshData.normals[3] = meshData.normals[4] = meshData.normals[5] =
		glm::vec3(0.0f, 1.0f, 0.0f);
	// Ceiling
	meshData.normals[6] = meshData.normals[7] = meshData.normals[8] =
		meshData.normals[9] = meshData.normals[10] = meshData.normals[11] =
		glm::vec3(0.0f, -1.0f, 0.0f);
	// Left wall
	meshData.normals[12] = meshData.normals[13] = meshData.normals[14] =
		meshData.normals[15] = meshData.normals[16] = meshData.normals[17] =
		glm::vec3(1.0f, 0.0f, 0.0f);
	// Right wall
	meshData.normals[18] = meshData.normals[19] = meshData.normals[20] =
		meshData.normals[21] = meshData.normals[22] = meshData.normals[23] =
		glm::vec3(-1.0f, 0.0f, 0.0f);
	// Near wall
	meshData.normals[24] = meshData.normals[25] = meshData.normals[26] =
		meshData.normals[27] = meshData.normals[28] = meshData.normals[29] =
		glm::vec3(0.0f, 0.0f, -1.0f);
	// Far wall
	meshData.normals[30] = meshData.normals[31] = meshData.normals[32] =
		meshData.normals[33] = meshData.normals[34] = meshData.normals[35] =
		glm::vec3(0.0f, 0.0f, 1.0f);

	// Indices
	for (unsigned int i = 0; i < 36; ++i)
		meshData.indices[i] = i;

	// Ranges
	MeshData::Range range;
	range.materialIndex = 0;
	range.vertexCount = 36;
	range.indexCount = 36;
	range.startVertex = 0;
	range.startIndex = 0;
	range.minBoundingBox = glm::vec3(-1.0f, -1.0f, -1.0f);
	range.maxBoundingBox = glm::vec3(1.0f, 1.0f, 1.0f);
	range.useBoundingBox = false;
	meshData.ranges.push_back(range);

	// Material
	MeshData::Material material;
	material.diffuseTextureIndex = -1;
	material.normalMapTextureIndex = -1;
	material.color = glm::vec4(0.95f, 0.9f, 0.7f, 1.0f);
	meshData.materials.push_back(material);

	// Model matrices
	meshData.model = glm::mat4(1.0f);
	meshData.modelInverse = glm::mat4(1.0f);

	int index = m_mesh.Load(meshData);
	m_mesh.RecalculateData(index, glm::mat4(glm::mat3(5.0f)));
}

void GLRenderer::InitTest()
{
	m_file.Init("test.csv");
	m_file.Write("ID,Bounces,Point Lights,Resolution,Vertices,Thread Group Size,Primary Ray Generation Time,Intersection Stage Time,Color Stage Time\n");

	m_testBounces.insert(m_testBounces.begin(), { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
	m_testPointLights.insert(m_testPointLights.begin(), { 0, 1, 2, 4, 8 });
	m_testResolutions.insert(m_testResolutions.begin(), {
		std::pair<int, int>(640, 480),
		std::pair<int, int>(800, 600),
		std::pair<int, int>(1024, 768),
		std::pair<int, int>(1920, 1080),
	});
	m_testModels.insert(m_testModels.begin(), { 0, 1, 2 });

	m_totalTestCount = (unsigned int)(m_testBounces.size() * m_testPointLights.size() *
		m_testResolutions.size() * m_testModels.size());

	m_primaryRayGenerationTimeSum = 0;
	m_intersectionStageTimeSum = 0;
	m_colorStageTimeSum = 0;
}

void GLRenderer::TickTest()
{
	if (m_testIndex >= m_totalTestCount)
		return;

	unsigned int testBounce;
	unsigned int testPointLight;
	std::pair<int, int> testResolution;
	unsigned int testModel;

	unsigned int index = 0;
	bool isDone = false;

	unsigned int size0 = (unsigned int)m_testPointLights.size();
	unsigned int size1 = (unsigned int)m_testBounces.size() * size0;
	unsigned int size2 = (unsigned int)m_testResolutions.size() * size1;
	unsigned int size3 = (unsigned int)m_testModels.size() * size2;

	unsigned int i0 = (m_testIndex / (size0 / (unsigned int)m_testPointLights.size())) % (unsigned int)m_testPointLights.size();
	unsigned int i1 = (m_testIndex / (size1 / (unsigned int)m_testBounces.size())) % (unsigned int)m_testBounces.size();
	unsigned int i2 = (m_testIndex / (size2 / (unsigned int)m_testResolutions.size())) % (unsigned int)m_testResolutions.size();
	unsigned int i3 = (m_testIndex / (size3 / (unsigned int)m_testModels.size())) % (unsigned int)m_testModels.size();

	testPointLight = m_testPointLights[i0];
	testBounce = m_testBounces[i1];
	testResolution = m_testResolutions[i2];
	testModel = m_testModels[i3];

	m_bounceCount = testBounce;
	m_pointLightCount = testPointLight;
	if (testResolution.first != m_width || testResolution.second != m_height)
		SetSize(testResolution.first, testResolution.second);
	if (testModel != m_modelMeshIndex)
	{
		m_modelMeshIndex = testModel;
		CreateModelRoomMesh(m_modelMeshInfos[m_modelMeshIndex]);
	}
}

void GLRenderer::CollectTestData(float primaryRayGenerationTime, float intersectionStageTime, float colorStageTime)
{
	if (m_testIndex >= m_totalTestCount)
		return;

	m_primaryRayGenerationTimeSum += primaryRayGenerationTime;
	m_intersectionStageTimeSum += intersectionStageTime;
	m_colorStageTimeSum += colorStageTime;

	if (m_frameIndex == 0)
	{
		m_file.Write(
			std::to_string(m_testIndex) + "," +
			std::to_string(m_bounceCount) + "," +
			std::to_string(m_pointLightCount) + "," +
			std::to_string(m_width) + "x" + std::to_string(m_height) + "," +
			std::to_string(m_mesh.GetVertexCount()) + "," +
			"8x8" /* THREAD GROUP SIZE HERE */ + "," +
			std::to_string(m_primaryRayGenerationTimeSum / 5.0f) + "," +
			std::to_string(m_intersectionStageTimeSum / 5.0f) + "," +
			std::to_string(m_colorStageTimeSum / 5.0f) + "\n");

		m_primaryRayGenerationTimeSum = 0.0f;
		m_intersectionStageTimeSum = 0.0f;
		m_colorStageTimeSum = 0.0f;
	}

	if (m_frameIndex >= 5)
	{
		std::cout << "Test " << m_testIndex << " of " << m_totalTestCount << " completed." << std::endl;
		m_testIndex++;
		m_frameIndex = 0;
	}
	else
		m_frameIndex++;
}