#ifndef GLRENDERER_H
#define GLRENDERER_H

#include <string>
#include <vector>
#include <iostream>

#include "SDLWindow.h"
#include "GLProgramHandler.h"
#include "GLFrameTexture.h"
#include "GLShaderBuffer.h"
#include "Camera.h"
#include "Mesh.h"
#include "SDLText.h"
#include "CSVFile.h"

class GLRenderer : public SDLWindow
{
public:
	bool Init(const std::string& name, int width, int height);

protected:
	void Update(float dt);
	void Render();

	void OnResize(int width, int height);
	void OnMouseMove(int dx, int dy);
	void OnKeyDown(SDL_Keycode key);

private:
	struct ModelMeshInfo
	{
		std::string filepath;
		glm::vec3 position;
		float scale;
	};

private:
	bool InitPrograms();

	void CreatePointLights();
	void CreateDirectionalLights();

	void CreateModelRoomMesh(ModelMeshInfo modelMeshInfo);
	void CreateRoomMesh();

	void InitTest();
	void TickTest();
	void CollectTestData(float primaryRayGenerationTime, float intersectionStageTime, float colorStageTime);

private:
	GLProgramHandler m_programHandler;
	Camera m_camera;

	GLFrameTexture m_frameTexture;
	GLShaderBuffer m_rayBuffer;
	GLShaderBuffer m_intersectionBuffer;
	GLShaderBuffer m_pointLightBuffer;
	GLShaderBuffer m_directionalLightBuffer;

	Mesh m_mesh;

	SDLText m_text;

	float m_currentFPS;

	unsigned int m_bounceCount;
	unsigned int m_pointLightCount;
	bool m_useSupersampling;

	unsigned int m_modelMeshIndex;
	std::vector<ModelMeshInfo> m_modelMeshInfos;

	// Test data
	CSVFile m_file;

	std::vector<unsigned int> m_testBounces;
	std::vector<unsigned int> m_testPointLights;
	std::vector<std::pair<int, int>> m_testResolutions;
	std::vector<unsigned int> m_testModels;

	float m_primaryRayGenerationTimeSum;
	float m_intersectionStageTimeSum;
	float m_colorStageTimeSum;

	unsigned int m_frameIndex;
	unsigned int m_testIndex;
	unsigned int m_totalTestCount;
};

#endif