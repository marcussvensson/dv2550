#include "GLShaderBuffer.h"

GLShaderBuffer::GLShaderBuffer()
{
	m_initialized = false;
}

GLShaderBuffer::~GLShaderBuffer()
{
	if (m_initialized)
		glDeleteBuffers(1, &m_buffer);
}

void GLShaderBuffer::Init(GLsizeiptr dataSize, GLenum usage)
{
	if (!m_initialized)
	{
		glGenBuffers(1, &m_buffer);
		m_initialized = true;
	}
	Resize(dataSize, usage);
}

void GLShaderBuffer::Resize(GLsizeiptr dataSize, GLenum usage)
{
	m_dataSize = dataSize;

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_buffer);
	glBufferData(GL_SHADER_STORAGE_BUFFER, dataSize, nullptr, usage);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void* GLShaderBuffer::Map()
{
	if (!m_initialized)
		return nullptr;

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_buffer);
	return glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, m_dataSize, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
}

void GLShaderBuffer::Unmap()
{
	if (!m_initialized)
		return;

	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void GLShaderBuffer::Bind(GLuint location)
{
	if (!m_initialized)
		return;

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, location, m_buffer);
}