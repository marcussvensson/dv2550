#ifndef ASSIMPLOADER_H
#define ASSIMPLOADER_H

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <vector>
#include <assimp/scene.h>
#include <map>

#include "MeshData.h"

class AssimpLoader
{
public:
	static AssimpLoader& GetInstance();

public:
	void Load(const std::string& filepath, MeshData* data);

private:
	AssimpLoader();
	AssimpLoader(AssimpLoader const&);
	void operator=(AssimpLoader const&);

	void LoadMaterials(const std::string& filepath, MeshData* meshData);
	void LoadTextureFilepath(const aiMaterial* pAiMaterial, aiTextureType aiType, const std::string& directory,
							 int& index, std::vector<std::string>& filepaths);

	void LoadVertices(const aiMesh* pAiMesh, MeshData* meshData,
					  glm::vec3* minBoundingBox, glm::vec3* maxBoundingBox);
	void LoadIndices(const aiMesh* pAiMesh, MeshData* meshData);
	void LoadBones(const aiMesh* pAiMesh, MeshData* meshData, unsigned int vertexOffset);

	void LoadNodeHierarchy(const aiNode* pAiNode, MeshData::Node& node);

	const aiNodeAnim* FindAiNodeAnim(const aiAnimation* animation, const std::string& nodeName);

	void LoadTranslationKeys(const aiNodeAnim* pAiNodeAnim, std::vector<MeshData::Node::Animation::TranslationKey>& translationKeys);
	void LoadRotationKeys(const aiNodeAnim* pAiNodeAnim, std::vector<MeshData::Node::Animation::RotationKey>& rotationKeys);
	void LoadScalingKeys(const aiNodeAnim* pAiNodeAnim, std::vector<MeshData::Node::Animation::ScalingKey>& scalingKeys);

private:
	// Temporary variables needed throughout the load process
	const aiScene* m_scene;
	std::map<std::string, unsigned int> m_boneMap;
	std::vector<glm::mat4> m_boneOffsets;
};

#endif