#ifndef SDLWINDOW_H
#define SDLWINDOW_H

#include <SDL.h>
#include <string>

class SDLWindow
{
public:
	SDLWindow();

	virtual bool Init(const std::string& name, int width, int height);

	void Run();

	void SetSize(int width, int height);

protected:
	void Quit();

	bool IsPressed(SDL_Scancode key) const;
	bool IsPressed(unsigned int button) const;

	virtual void Update(float dt) = 0;
	virtual void Render() = 0;

	virtual void OnResize(int width, int height);
	virtual void OnMouseMove(int dx, int dy);
	virtual void OnKeyDown(SDL_Keycode key);

protected:
	int m_width,
		m_height;

private:
	SDL_Window* m_window;
	SDL_GLContext m_context;
	bool m_run;

	const Uint8* m_currentKeyboardState;
	Uint32 m_currentMouseState;
	int m_previousCursorPositionX,
		m_previousCursorPositionY;
};

#endif