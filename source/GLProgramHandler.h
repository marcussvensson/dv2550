#ifndef GLPROGRAMHANDLER_H
#define GLPROGRAMHANDLER_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>
#include <map>

class GLProgramHandler
{
public:
	GLProgramHandler();
	~GLProgramHandler();

	struct ShaderFile
	{
		std::string filepath;
		GLenum type;
	};
	struct ShaderSource
	{
		std::string source;
		GLenum type;
	};
	void AddFromFile(const std::string& programName, const ShaderFile* shaderFiles, unsigned int shaderFileCount);
	void AddFromSource(const std::string& programName, const ShaderSource* shaderSources, unsigned int shaderSourceCount);

	void PrefetchUniforms(const std::string& programName, const std::string* uniformNames, unsigned int uniformNameCount);

	void Use(const std::string& programName);

	void SetUniform(const std::string& name, int value);
	void SetUniform(const std::string& name, unsigned int value);
	void SetUniform(const std::string& name, float value);
	void SetUniform(const std::string& name, glm::vec2 value);
	void SetUniform(const std::string& name, glm::vec3 value);
	void SetUniform(const std::string& name, glm::vec4 value);
	void SetUniform(const std::string& name, glm::mat2 value);
	void SetUniform(const std::string& name, glm::mat3 value);
	void SetUniform(const std::string& name, glm::mat4 value);
	void SetUniform(const std::string& name, const int* value, unsigned int count);
	void SetUniform(const std::string& name, const unsigned int* value, unsigned int count);
	void SetUniform(const std::string& name, const float* value, unsigned int count);
	void SetUniform(const std::string& name, const glm::vec2* value, unsigned int count);
	void SetUniform(const std::string& name, const glm::vec3* value, unsigned int count);
	void SetUniform(const std::string& name, const glm::vec4* value, unsigned int count);
	void SetUniform(const std::string& name, const glm::mat2* value, unsigned int count);
	void SetUniform(const std::string& name, const glm::mat3* value, unsigned int count);
	void SetUniform(const std::string& name, const glm::mat4* value, unsigned int count);

private:
	GLint GetUniformLocation(const std::string& name);

private:
	std::map<std::string, std::pair<GLuint, int>> m_programs;
	GLuint m_currentProgram;
	std::map<GLuint, std::map<std::string, GLint>> m_programUniformNameLocations;
};

#endif