#include "Octree.h"

Octree::AABB::AABB()
{
	bottomLeftCorner = glm::vec3(0.0f, 0.0f, 0.0f);
	topRightCorner = glm::vec3(0.0f, 0.0f, 0.0f);
	center = glm::vec3(0.0f, 0.0f, 0.0f);
}
Octree::AABB::AABB(glm::vec3 p0, glm::vec3 p1)
{
	bottomLeftCorner = glm::vec3(std::min(p0.x, p1.x), std::min(p0.y, p1.y), std::min(p0.z, p1.z));
	topRightCorner = glm::vec3(std::max(p0.x, p1.x), std::max(p0.y, p1.y), std::max(p0.z, p1.z));
	center = 0.5f * bottomLeftCorner + 0.5f * topRightCorner;
}
Octree::AABB::AABB(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2)
{
	bottomLeftCorner = glm::vec3(std::min(std::min(p0.x, p1.x), p2.x), std::min(std::min(p0.y, p1.y), p2.y), std::min(std::min(p0.z, p1.z), p2.z));
	topRightCorner = glm::vec3(std::max(std::max(p0.x, p1.x), p2.x), std::max(std::max(p0.y, p1.y), p2.y), std::max(std::max(p0.z, p1.z), p2.z));
	center = 0.5f * bottomLeftCorner + 0.5f * topRightCorner;
}
Octree::AABB::AABB(glm::vec4 p0, glm::vec4 p1)
{
	bottomLeftCorner = glm::vec3(std::min(p0.x, p1.x), std::min(p0.y, p1.y), std::min(p0.z, p1.z));
	topRightCorner = glm::vec3(std::max(p0.x, p1.x), std::max(p0.y, p1.y), std::max(p0.z, p1.z));
	center = 0.5f * bottomLeftCorner + 0.5f * topRightCorner;
}
Octree::AABB::AABB(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2)
{
	bottomLeftCorner = glm::vec3(std::min(std::min(p0.x, p1.x), p2.x), std::min(std::min(p0.y, p1.y), p2.y), std::min(std::min(p0.z, p1.z), p2.z));
	topRightCorner = glm::vec3(std::max(std::max(p0.x, p1.x), p2.x), std::max(std::max(p0.y, p1.y), p2.y), std::max(std::max(p0.z, p1.z), p2.z));
	center = 0.5f * bottomLeftCorner + 0.5f * topRightCorner;
}

void Octree::AABB::Union(AABB aabb)
{
	bottomLeftCorner.x = std::min(bottomLeftCorner.x, aabb.bottomLeftCorner.x);
	bottomLeftCorner.y = std::min(bottomLeftCorner.y, aabb.bottomLeftCorner.y);
	bottomLeftCorner.z = std::min(bottomLeftCorner.z, aabb.bottomLeftCorner.z);
	topRightCorner.x = std::max(topRightCorner.x, aabb.topRightCorner.x);
	topRightCorner.y = std::max(topRightCorner.y, aabb.topRightCorner.y);
	topRightCorner.z = std::max(topRightCorner.z, aabb.topRightCorner.z);
	center = 0.5f * bottomLeftCorner + 0.5f * topRightCorner;
}

bool Octree::AABB::IsInside(glm::vec3 p)
{
	if (p.x >= bottomLeftCorner.x && p.x <= topRightCorner.x &&
		p.y >= bottomLeftCorner.y && p.y <= topRightCorner.y &&
		p.z >= bottomLeftCorner.z && p.z <= topRightCorner.z)
		return true;
	return false;
}
bool Octree::AABB::IsInside(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2)
{
	if (IsInside(p0) &&
		IsInside(p1) &&
		IsInside(p2))
		return true;
	return false;
}
bool Octree::AABB::IsInside(glm::vec4 p)
{
	return IsInside(glm::vec3(p));
}
bool Octree::AABB::IsInside(glm::vec4 p0, glm::vec4 p1, glm::vec4 p2)
{
	return IsInside(glm::vec3(p0), glm::vec3(p1), glm::vec3(p2));
}

Octree::Octree()
{
	m_vertexCount = 0;
}

void Octree::AddVertices(std::vector<Vertex>* vertices, unsigned int minTrianglesPerNode,
						 unsigned int& baseNode, unsigned int& nodeCount)
{
	m_minTrianglesPerNode = minTrianglesPerNode;

	AABB octreeAABB;
	const unsigned int vertexCount = (unsigned int)vertices->size();
	for (unsigned int i = 0; i < vertexCount; i += 3)
		octreeAABB.Union(AABB(vertices->at(i).position,
							  vertices->at(i + 1).position,
							  vertices->at(i + 2).position));

	BuildNode root;
	root.aabb = octreeAABB;

	m_buildNodes.clear();
	m_buildNodes.push_back(root);
	RecursiveBuild(0, *vertices);

	vertices->clear();
	//m_nodes.clear();
	baseNode = (unsigned int)m_nodes.size();
	nodeCount = baseNode + (unsigned int)m_buildNodes.size();
	m_nodes.resize(m_nodes.size() + m_buildNodes.size());
	unsigned int temp = 0;
	RecursiveNodeCreation(0, baseNode, temp, vertices);

	m_vertexCount += (unsigned int)vertices->size();
}

void Octree::Build()
{
	unsigned int nodeCount = (unsigned int)m_nodes.size();
	m_nodeBuffer.Init(nodeCount * 48, GL_STATIC_COPY);
	Node* nodes = (Node*)m_nodeBuffer.Map();
	for (unsigned int i = 0; i < nodeCount; ++i)
		nodes[i] = m_nodes[i];
	m_nodeBuffer.Unmap();
}

void Octree::Bind(GLuint location)
{
	m_nodeBuffer.Bind(location);
}

void Octree::Reset()
{
	m_nodes.clear();
	m_vertexCount = 0;
}

void Octree::RecursiveBuild(int nodeIndex, std::vector<Vertex> vertices)
{
	BuildNode* node = &m_buildNodes[nodeIndex];
	BuildNode c[8];
	std::vector<Vertex> v[8];

	// If minimum number of triangles are reached, end building this node
	const unsigned int vertexCount = (unsigned int)vertices.size();
	if (vertexCount / 3 <= m_minTrianglesPerNode)
	{
		node->vertices = vertices;
		node->children[0] = -1;
		return;
	}

	// Create children AABBs from node
	CreateBuildNodeAABBs(node, c);
	AABB ca[8];
	for (unsigned int i = 0; i < 8; ++i)
		ca[i] = c[i].aabb;

	// Place triangles in children, if they don't fit put them in this node
	for (unsigned int i = 0; i < vertexCount; i += 3)
	{
		Vertex v0 = vertices[i];
		Vertex v1 = vertices[i + 1];
		Vertex v2 = vertices[i + 2];

		for (unsigned int j = 0; j < 8; ++j)
			if (ca[j].IsInside(v0.position) ||
				ca[j].IsInside(v1.position) ||
				ca[j].IsInside(v2.position))
			{
				AABB temp = c[j].aabb;
				temp.Union(AABB(v0.position, v1.position, v2.position));
				if (temp.bottomLeftCorner != node->aabb.bottomLeftCorner ||
					temp.topRightCorner != node->aabb.topRightCorner)
				{
					AddTriangle(&v[j], v0, v1, v2);
					c[j].aabb.Union(AABB(v0.position, v1.position, v2.position));
					break;
				}
			}
	}

	// Build children recursively
	for (unsigned int i = 0; i < 8; ++i)
	{
		m_buildNodes.push_back(c[i]);

		node = &m_buildNodes[nodeIndex];
		node->children[i] = (int)m_buildNodes.size() - 1;
		RecursiveBuild(node->children[i], v[i]);
	}
}

void Octree::CreateBuildNodeAABBs(BuildNode* node, BuildNode children[8])
{
	glm::vec3 bl = node->aabb.bottomLeftCorner;
	glm::vec3 tr = node->aabb.topRightCorner;
	glm::vec3 c = node->aabb.center;

	glm::vec3 axes = glm::abs(node->aabb.topRightCorner - node->aabb.center);
	glm::vec3 r = glm::vec3(1.0f, 0.0f, 0.0f) * axes; // Right
	glm::vec3 u = glm::vec3(0.0f, 1.0f, 0.0f) * axes; // Up
	glm::vec3 f = glm::vec3(0.0f, 0.0f, 1.0f) * axes; // Forward

	children[0].aabb = AABB(bl, c);
	children[1].aabb = AABB(bl + r, c + r);
	children[2].aabb = AABB(bl + f, c + f);
	children[3].aabb = AABB(bl + r + f, c + r + f);
	children[4].aabb = AABB(c, tr);
	children[5].aabb = AABB(c - r, tr - r);
	children[6].aabb = AABB(c - f, tr - f);
	children[7].aabb = AABB(c - r - f, tr - r - f);
}

void Octree::AddTriangle(std::vector<Vertex>* vertices, Vertex v0, Vertex v1, Vertex v2)
{
	vertices->push_back(v0);
	vertices->push_back(v1);
	vertices->push_back(v2);
}

void Octree::RecursiveNodeCreation(unsigned int nodeIndex, unsigned int nodeOffset, unsigned int& nodeCount, std::vector<Vertex>* vertices)
{
	Node* node = &m_nodes[nodeIndex + nodeOffset];
	BuildNode* buildNode = &m_buildNodes[nodeIndex];

	node->bottomLeftCorner = glm::vec4(buildNode->aabb.bottomLeftCorner, 0.0f);
	node->topRightCorner = glm::vec4(buildNode->aabb.topRightCorner, 0.0f);

	node->baseVertex = m_vertexCount + (unsigned int)vertices->size();
	node->vertexCount = buildNode->vertices.empty() ? 0 : node->baseVertex + (unsigned int)buildNode->vertices.size();
	vertices->insert(vertices->end(), buildNode->vertices.begin(), buildNode->vertices.end());

	unsigned int prevNodeCount = nodeCount;
	nodeCount++;
	if (buildNode->children[0] != -1)
		for (unsigned int i = 0; i < 8; ++i)
			RecursiveNodeCreation(buildNode->children[i], nodeOffset, nodeCount, vertices);
	node->childOffset = nodeCount - prevNodeCount;
}