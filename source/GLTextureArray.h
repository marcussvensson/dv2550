#ifndef GLTEXTUREARRAY_H
#define GLTEXTUREARRAY_H

#include <vector>
#include <string>
#include <GL/glew.h>

class GLTextureArray
{
public:
	~GLTextureArray();

	void Init(std::vector<std::string> filepaths);
	
	void Bind();
	void Bind(GLint index);

private:
	GLuint m_texture;
};

#endif